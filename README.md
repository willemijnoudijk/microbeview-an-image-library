# MicrobeView

## Introduction
The MicrobeView Application is an application in which you can browse through an image library and modify images for laboratory work. MicrobeView was created for Lambert Instruments, which is a company specialized in electronic imaging. The company offers intensified CCD cameras to institutes that are active, among other things, in biomedical research, such as Hanzehogeschool laboratory students. These cameras are very sensitive to light, they use high-speed imaging, fluorescence imaging and scientific imaging and create images of very high quality. For a company like Lambert Instruments, the tool MicrobeView will be very useful. MicrobeView
ensures that a customer of the company can easily control the groups images. MicrobeView is an user-friendly product that provides easy and delicate methods of controlling the library. A lot of features are provided, that will make any file directory easier to manage and edit. To take a closer look at the available features please take a look at (Featurs)[./ProjectImageLibraryUI_Doelstellingen.pdf]. The tool will accessible for a whole team and is strongly protected against outsiders so the image library will always be save. 
LibraryView is still in production but will be an excellent investment when it is finished!

## Prerequisites
To use MicrobeView please make sure you have: 

* IntelliJ: [IntelliJ download page](https://www.jetbrains.com/idea/download/#section=linux)
* Java: [Java download page](https://www.java.com/nl/download/)
* Apache Tomcat: [Tomcat download page](https://tomcat.apache.org/download-80.cgi)

## Usage
1. Clone this repository
2. Open the build.gradle in IntelliJ
3. Configure the Tomcat server (go to settings > Build, Execution, Deployment > Application servers)
4. Adjust the application.properties to your likings
5. Start the application by running the ImageLibraryApplication in the folder ‘nl.bioinf.imagelibrary’

## Built with
* [Java 1.8](https://en.wikipedia.org/wiki/Java_(programming_language)) as programming language for the backend
* [Javascript 1.8.5](https://en.wikipedia.org/wiki/JavaScript) as programming language to make the application dynamic
* [HTML 5](https://en.wikipedia.org/wiki/HTML) & [Thymeleaf 3.0.11](https://en.wikipedia.org/wiki/Thymeleaf) for frontend creation
* [CSS 4](https://en.wikipedia.org/wiki/Cascading_Style_Sheets) for design
* [Spring boot framework 2.1.1](https://en.wikipedia.org/wiki/Spring_Framework) as the framework for building the application

## Built in API
Following complications during creation of a stand-alone API (micro-service) for the course Webbased 
Information Systems 2, it has been integrated into this project. The classes which run the actual API are:

* MetadataRestController.java
* MetadataRestControllerTest.java

The MetadataRestController is a Rest-Controller which for now has three different endpoints:  

* /api/metadata/v1/get/by/id  
for retrieving the metadata by a database identifier
* /api/metadata/v1/get/by/source  
for retrieving all metadata for a specific image
* /api/metadata/v1/validate/tag  
for validating a tag by using various parameters to find the right instance in the database

The latter two are also used in AJAX request in this very project. 
In the future, the API could be used to perform many more GET and POST requests concerning the metadata, additional to the three stated above.

Also, completely building (via bootJar) the project results in the documentation and thus generation of request and response snippets.
The snippets of one particular test can be looked at as an example by generating a HTML file by running the asciidoctor gradle task.
On that page the request and the response will be clearly shown for better understanding.

## Authors
* Olle de Jong | [work](https://bitbucket.org/olledejong/)
* Kylie Keijzer | [work](https://bitbucket.org/kyliekeijzer/)
* Willemijn Oudijk | [work](https://bitbucket.org/willemijnoudijk/)

## Acknowledgments
Jasper Bosch from [Lambert Instruments](https://www.lambertinstruments.com/)

## Screenshot
![screenshot](/src/main/resources/static/images/screenshot1.png)




