/**
 *
 * NOT FINISHED
 * This is the start for creating the search functionality of tags. The images should show up.
 * @author Willemijn
 */

$(document).ready(function () {
    $(".submit_search").click(function(e) {
        e.preventDefault();

        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");

        var form = document.getElementById("searchForm");
        console.log("should be test1", document.getElementById("search").accessKey);
      //  getImageOnTagSearch(form);
        var formData1 = new FormData(form);
        var xhr = new XMLHttpRequest();
        //var formData2 = new FormData();
        //console.log("works?", form.elements["search"]);

        xhr.open('POST', '/getmetadata');
        xhr.setRequestHeader(header, token);
        xhr.send(formData1);
        getImageOnTagSearch();
    });

    function getImageOnTagSearch() {
        console.log();
        $.ajax({
            type: "GET",
            contentType: "application/json",
            dataType: "json",
            url: "/getmetadata/tag",
            success: function (data) {
            console.log("inside success of image tag search");
                console.log(data)
            },
            error: function (xhr, status) {

            }
        });
    }


    /**
     * Search functionality for searching for a tag of all the images
     * NOT finished, needs to be optimalized.
     * The field with ID search is commented out in the home.html page for now.
     */
    $("#search").keyup(function() {
        var searchInput = document.getElementById("search");
        var filter = searchInput.value.toUpperCase();
        //var creator = document.getElementsByClassName("creator-cell-inp");
        //console.log("creator leng", creator.length);
        $.ajax({
            type: "GET",
            contentType: "application/json",
            dataType: "json",
            url: "/getmetadata/"+filter,
            success: function (data) {
                $("#directory_files").hidden;
                console.log("inside success of image tag search");
                console.log(data);
                for (var i=0; i < data.length; i++) {
                    var filename = data[i];
                    console.log(filename);
                    // Get original image name of thumbnail image
                    //var image_name = filename.substr(0, filename.indexOf("_thumb"));
                    var Length = filename.length;
                    var extensionStart = filename.indexOf(".");
                    var image_extension = filename.substr(extensionStart, Length);
                    var name = filename.substring(0, extensionStart);
                    //console.log(name);
                    // The imagename without the _thumb
                    var image_src = name+"_thumb";
                    var source = image_src.concat(image_extension);
                    console.log("thumb source", source);
                    if (filename.length > 18) {
                        var part1 = filename.substring(0, 8);
                        var part2 = filename.substring(image_src.length - 8, image_src.length);
                        var new_img_src = part1 + ".." + part2;

                    } else {
                        new_img_src = image_src;
                    }
                    var path = "C:/Users/willemijnoudijk/Pictures/MinorImages/MicrobeView/";
                    // this is for linux: path starts with slash

                    if (path.charAt(0) === "/") {

                        $('#directory_files2').append(
                            '<div class="column"><img src="' +
                            path + source + ' " onclick="expandClickedImage(this);"/><br><p class="filename">' +
                            new_img_src + '</p></div>');
                    } // this is for windows: path starts without a slash
                    else {
                        $('#directory_files2').append(
                            '<div class="column"><img src="' +
                            "/" + path + source + ' " onclick="expandClickedImage(this);"/><br><p class="filename">' +
                            new_img_src + '</p></div>');
                    }
                }
            },
            error: function (xhr, status) {
            }
        });
    })

});