/**
 * Method that creates functionality for deleting a selected folder
 *
 * @author Kylie
 */
function deleteDirectory(selected_folder) {
    var xhr = new XMLHttpRequest();
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    var params = "selectedFolder="+selected_folder;

    console.log("Deleted folder '" + selected_folder + "' with all its content");

    xhr.open('POST', '/folderdeleting', true);
    xhr.setRequestHeader(header, token);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(params);

    $('#dir_options').hide();

    xhr.onreadystatechange = function() {
        if (xhr.status === 200) {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                getUpdatedData(current_folder, current_direction, current_history,
                    current_main_folder, current_main_data);
            }
        } else if (xhr.status === 500) {
            $(".div_left").append(
                '<div id="error-msg-diradd" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;Error; there is something wrong with deleting the folder ' +selected_folder+ '.</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        } else {
            $(".div_left").append(
                '<div id="error-msg-diradd" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;Error deleting folder ' + selected_folder + '</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        }
    }
}