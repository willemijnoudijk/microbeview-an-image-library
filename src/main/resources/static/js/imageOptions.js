/**
 * Methods that create functionality for supporting different image options,
 * such as renaming and deleting.
 *
 * @author Kylie
 */

/**
 * Function that renames the selected images to the new given image name
 * @param selected_image
 * @param thumbnail_src
 * @param prefix
 * @param new_image_name
 */
function renameImage(selected_image, thumbnail_src, prefix, new_image_name) {
    selected_image = selected_image.split(prefix)[1];
    thumbnail_src = thumbnail_src.split(prefix)[1];

    var xhr = new XMLHttpRequest();
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    var params = "selectedImage="+selected_image+"&thumbnailSource="+thumbnail_src+"&newImageName="+new_image_name;

    xhr.open('POST', '/imagerenaming', true);
    xhr.setRequestHeader(header, token);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(params);

    $('#img_options').hide();
    $('#dir_options').show();

    console.log("Renamed image '" + selected_image.split(/[/\\]+/).pop() + "' to: '" + new_image_name + "'");

    xhr.onreadystatechange = function() {
        if (xhr.status === 200) {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                getUpdatedData(current_folder, current_direction, current_history,
                    current_main_folder, current_main_data);
            }
        } else if (xhr.status === 500) {
            $(".div_left").append(
                '<div id="error-msg-diradd" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;Error; there is something wrong with renaming the image ' +selected_image+ '.</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        } else {
            $(".div_left").append(
                '<div id="error-msg-diradd" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;Error renaming image' + selected_image + '</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        }
    }
}

/**
 * Function that checks if the given image/folder name is valid.
 * @param new_image_name
 * @returns {boolean}
 */
function isValidName(new_image_name) {
    var chars = /[ !@#$%^&*+=\[\]{};':"\\|,.\/?]/;
    if (chars.test(new_image_name)) {
        return false;
    }
    else {
        return true;
    }
}

/**
 * Function that deletes the selected image.
 * @param selected_image
 * @param thumbnail_src
 * @param prefix
 */
function deleteImage(selected_image, thumbnail_src, prefix) {

    selected_image = selected_image.split(prefix)[1];
    thumbnail_src = thumbnail_src.split(prefix)[1];

    var xhr = new XMLHttpRequest();
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    var params = "selectedImage="+selected_image+"&thumbnailSource="+thumbnail_src;

    console.log("Deleted image '" + selected_image + "'");

    xhr.open('POST', '/imagedeleting', true);
    xhr.setRequestHeader(header, token);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(params);

    $('#img_options').hide();
    $('#dir_options').show();

    xhr.onreadystatechange = function() {
        if (xhr.status === 200) {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                getUpdatedData(current_folder, current_direction, current_history,
                    current_main_folder, current_main_data);
            }
        } else if (xhr.status === 500) {
            $(".div_left").append(
                '<div id="error-msg-diradd" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;Error; there is something wrong with deleting the image ' +selected_image+ '.</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        } else {
            $(".div_left").append(
                '<div id="error-msg-diradd" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;Error deleting image' + selected_image + '</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        }
    }
}
