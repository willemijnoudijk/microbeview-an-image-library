/**
 * @author Olle
 */
// FILE JUST FOR METADATA STORAGE TESTING ON metadata.html TEMPLATE
$(document).ready(function() {
    var submitbutton = document.getElementById("insert_submit");
    var submitbutton2 = document.getElementById("insert_submit2");

    submitbutton.onclick = function storeMetadata(e) {
        e.preventDefault();
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        var form = document.getElementById("metadataform");

        var formData = new FormData(form);
        var xhr = new XMLHttpRequest();

        xhr.open('POST', '/storexymetadata');
        xhr.setRequestHeader(header, token);
        xhr.send(formData);

        xhr.onreadystatechange = function () {
            console.log(this.readyState);
            console.log(this.status);
            if (this.readyState === 4) {
                if (this.status === 200) {
                    $("body").append(
                        '<div id="success-msg-meta-test" class="success-msg">' +
                        '<i class="fa fa-check"></i>' +
                        ' You succesfully stored the test metadata' +
                        '</div>');
                    $(".success-msg").delay(400).fadeIn();
                    $(".success-msg").delay(5000).fadeOut();
                } else if (this.status === 500) {
                    $("body").append(
                        '<div id="error-msg-meta-test" class="error-msg"> ' +
                        '<i class="fa fa-times-circle"></i>&nbsp;Error; there is something wrong with storing the test metadata.</div>');
                    $(".error-msg").delay(400).fadeIn();
                    $(".error-msg").delay(5000).fadeOut();
                } else {
                    var spltIndx = this.responseText.indexOf("message") + 10;
                    var endInx = this.responseText.indexOf("trace") - 3;
                    var messageToSplit = this.responseText.substring(spltIndx, endInx);
                    //var finalMessage = messageToSplit.split(/;(.+)/)[0];
                    //console.log("finalmes", finalMessage);
                    $("body").append(
                        '<div id="error-msg-meta-test" class="error-msg"> ' +
                        '<i class="fa fa-times-circle"></i>&nbsp;Error; ' + messageToSplit + '</div>');
                    $(".error-msg").delay(400).fadeIn();
                    $(".error-msg").delay(5000).fadeOut();
                }
            }

        };
        $('.error-msg').remove();
        $('.success-msg').remove();
        form.reset();
    };




    submitbutton2.onclick = function storeMetadata(e) {
        e.preventDefault();
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        var form = document.getElementById("metadataform2");

        var formData = new FormData(form);
        var xhr = new XMLHttpRequest();

        xhr.open('POST', '/storegeneralmetadata');
        xhr.setRequestHeader(header, token);
        xhr.send(formData);
        xhr.onreadystatechange = function () {
            console.log(this.readyState);
            console.log(this.status);
            if (this.readyState === 4) {
                if (this.status === 200) {
                    $("body").append(
                        '<div id="success-msg-meta-test" class="success-msg">' +
                        '<i class="fa fa-check"></i>' +
                        ' You succesfully stored the test general metadata' +
                        '</div>');
                    $(".success-msg").delay(400).fadeIn();
                    $(".success-msg").delay(5000).fadeOut();
                } else if (this.status === 500) {
                    $("body").append(
                        '<div id="error-msg-meta-test" class="error-msg"> ' +
                        '<i class="fa fa-times-circle"></i>&nbsp;Error; there is something wrong with storing the test general metadata.</div>');
                    $(".error-msg").delay(400).fadeIn();
                    $(".error-msg").delay(5000).fadeOut();
                } else {
                    var spltIndx = this.responseText.indexOf("message") + 10;
                    var endInx = this.responseText.indexOf("trace") - 3;
                    var messageToSplit = this.responseText.substring(spltIndx, endInx);
                    //var finalMessage = messageToSplit.split(/;(.+)/)[0];
                    //console.log("finalmes", finalMessage);
                    $("body").append(
                        '<div id="error-msg-meta-test" class="error-msg"> ' +
                        '<i class="fa fa-times-circle"></i>&nbsp;Error; ' + messageToSplit + '</div>');
                    $(".error-msg").delay(400).fadeIn();
                    $(".error-msg").delay(5000).fadeOut();
                }
            }

        };
        $('.error-msg').remove();
        $('.success-msg').remove();

        form.reset();
    };
});