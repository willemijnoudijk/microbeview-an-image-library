/**
 * Method that creates functionality for creating a new directory in the current folder
 *
 * @author Kylie
 */
function addDirectory(new_folder, current_obj, current_direction, current_history,
                      current_main_folder, current_main_data) {

        var xhr = new XMLHttpRequest();
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        var params = "currentFolder="+current_folder+"&newFolder="+new_folder;

        console.log("Created new folder '" +new_folder+ "' in directory '" +current_folder+"'");

        xhr.open('POST', '/foldercreation', true);
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(params);

        xhr.onreadystatechange = function() {
            console.log(xhr.status);
            if (xhr.status === 200) {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    getUpdatedData(current_folder, current_direction, current_history,
                        current_main_folder, current_main_data);
                }
            }
            else if (xhr.status === 500) {
                $(".div_left").append(
                    '<div id="error-msg-diradd" class="error-msg"> ' +
                    '<i class="fa fa-times-circle"></i>&nbsp;Error; there is something wrong with adding the folder ' +new_folder+ '.</div>');
                $(".error-msg").delay(400).fadeIn();
                $(".error-msg").delay(5000).fadeOut();
            } else {
                $(".div_left").append(
                    '<div id="error-msg-diradd" class="error-msg"> ' +
                    '<i class="fa fa-times-circle"></i>&nbsp;Error adding folder ' + new_folder + '</div>');
                $(".error-msg").delay(400).fadeIn();
                $(".error-msg").delay(5000).fadeOut();
            }
        }
}