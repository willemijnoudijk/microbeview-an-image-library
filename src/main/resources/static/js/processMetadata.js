/**
 * @author Olle
 */
$(document).ready(function () {

    //**  VARIABLES  **//
    var submitbut_metadata = document.getElementsByClassName("submit_metadata");
    var submitbut_gen_metadata = document.getElementById("submit_metadata_general");
    var submitbut_xy_metadata = document.getElementById("submit_metadata");
    var create_pos_tag = document.getElementById("create_xytag");
    var create_gen_tag = document.getElementById("create_generaltag");

    /**
     * @function show metadata form when click on "ADD POSITIONAL IMAGE TAG"
     */
    create_pos_tag.onclick = function showXyTagForm() {
        var gen_form = document.getElementById("metadataform_general");
        gen_form.reset();
        $("#keyValueDiv").hide();
        $("#keyvalue-info").hide();
        $(".keyvalue-table-body").html("");
        $("#togBtn3").prop("checked", false);
        $(".image-info").hide();
        $("#togBtn2").prop('checked', false);
        $("#Metadata2").css("display", "none");
        $("#tagged").css("display", "block");
        $(".click_on_image").css("display", "block");
        validateInput();
        $('#taginp').change(validateInput);
        $(".table-body").html("");
    };

    /**
     * @function show metadata form when click on "ADD GENERAL IMAGE TAG"
     */
    create_gen_tag.onclick = function showGeneralTagForm() {
        var xy_form = document.getElementById("metadataform");
        xy_form.reset();

        $("#keyValueDiv").hide();
        $("#keyvalue-info").hide();
        $(".keyvalue-table-body").html("");
        $("#togBtn3").prop("checked", false);
        $(".image-info").hide();
        $("#togBtn2").prop('checked', false);

        $("#tagged").css("display", "none");
        $(".click_on_image").css("display", "none");

        $("#Metadata").css("display", "none");
        $("#Metadata2").css("display", "block");
        validateInput();
        $('#taginp_general').change(validateInput);
        $(".table-body").html("");

    };


    /**
     * @function stores POSITIONAL (x + y) metadata
     */
    submitbut_xy_metadata.onclick = function storeXyMetadata(e) {
        e.preventDefault();
        $(submitbut_xy_metadata).css("background-color", "grey");

        console.log("INSIDE storeXyMetadata FUNCTION");
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");

        //bevat alleen tagnaam
        var form = document.getElementById("metadataform");

        //bevat coordinaten
        var tagged = document.getElementById("tagged");
        var style = window.getComputedStyle(tagged);

        var left = style.getPropertyValue('left');
        var newleft = left.replace(/\D/g,'');

        var top = style.getPropertyValue('top');
        var newtop = top.replace(/\D/g,'');

        var fraction_of_height = (parseInt(newtop) / $(".expandedImg").height());
        var fraction_of_width = (parseInt(newleft) / $(".expandedImg").width());

        var image = document.getElementById("expandedImg");
        console.log("image source= " + image.src.substring(image.src.lastIndexOf('/') + 1));

        var formData1 = new FormData(form);
        var xhr = new XMLHttpRequest();
        //var formData2 = new FormData();
        formData1.append('leftpx', fraction_of_width);
        formData1.append('toppx', fraction_of_height);
        formData1.append('imageName', image.src.substring(image.src.lastIndexOf('/') + 1));

        /**
         * @function resets the pointer for adding a
         * positional (x and y) metadata tag
         */
        var name = 'store_metadata';
        var url  = '/storexymetadata';
        var succes_message = 'You successfully created the tag: [ ' + formData1.get("tag") + ' ] !';
        var error_message = 'Error: there might be something wrong with your input, please try again';
        //performPost(name, url, formData1, succes_message, error_message, e);

        $.ajax({
            url: '/storexymetadata',
            beforeSend: function(request) {
                request.setRequestHeader(header, token);
            },
            data: formData1,
            processData: false,
            contentType: false,
            type: 'POST'
        }).done(function( data ) {
            $(".div_right").append(
                '<div id="success-msg-xy" class="success-msg">' +
                '<i class="fa fa-check"></i>' +
                ' You successfully created the tag: [ ' + formData1.get("tag") + ' ] !' +
                '</div>');
            $(".success-msg").delay(400).fadeIn();
            $(".success-msg").delay(5000).fadeOut();
            getMetadataOnImage();
            $("#tagged").css("left", "0");
            $("#tagged").css("top", "0");
        }).fail(function( data ) {
            $(".div_right").append(
                '<div id="error-msg-xy" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;Error: there might be something wrong with your input, please try again</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        });

        $('.error-msg').remove();
        $('.success-msg').remove();
        form.reset();
    };

    /**
     * @function stores GENERAL metadata
     */
    submitbut_gen_metadata.onclick = function storeGenMetadata(e) {
        e.preventDefault();
        $(submitbut_gen_metadata).css("background-color", "grey");
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");

        var form = document.getElementById("metadataform_general");

        var image = document.getElementById("expandedImg");

        var formData1 = new FormData(form);
        formData1.append('imageName', image.src.substring(image.src.lastIndexOf('/') + 1));

        $.ajax({
            url: '/storegeneralmetadata',
            beforeSend: function(request) {
                request.setRequestHeader(header, token);
            },
            data: formData1,
            processData: false,
            contentType: false,
            type: 'POST'
        }).done(function( data ) {
            $(".div_right").append(
                '<div id="success-msg-gen" class="success-msg">' +
                '<i class="fa fa-check"></i>' +
                ' You successfully created the general tag: [ ' + formData1.get("tag") + ' ] !' +
                '</div>');
            $(".success-msg").delay(400).fadeIn();
            $(".success-msg").delay(5000).fadeOut();
        }).fail(function( data ) {
            $(".div_right").append(
                '<div id="error-msg-gen" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;Error: there is something wrong with your input, please try again</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        });

        $('.error-msg').remove();
        $('.success-msg').remove();
        form.reset();
    };


    /**
     * @function validate if there is text in input field
     *
     * WHEN INPUT: SUBMIT BUTTON ENABLED
     * WHEN NO INPUT: SUBMIT BUTTON DISABLED
     */
    function validateInput() {
        if ($('.tag_input').val().length > 0 || $('#taginp_general').val().length > 0) {
            console.log(".tag_input val > 0");
            $(submitbut_metadata).prop("disabled", false);
            $(submitbut_metadata).css("background-color", "#39ae3f");
            $(submitbut_metadata).css("cursor", "pointer")

        }
        else {
            $(submitbut_metadata).prop("disabled", true);
            $(submitbut_metadata).css("background-color", "grey");
            $(submitbut_metadata).css("cursor", "default")

        }
    }
});