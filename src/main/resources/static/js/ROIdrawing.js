/**
 * Methods that serve the functionality for creating a ROI from an image.
 * @author Kylie
 */

var cropper;
var original_image;
var file_name;

var image_canvas;
var ROIs = [];
var container;

var current_img;

$(document).ready(function () {

    /**
     * Creates the possibility for drawing a rectangle ROI
     */
    $('#draw_rect_roi').click(function (e) {

        $("#keyValueDiv").hide();
        e.preventDefault();
        $("#togBtn").prop("checked", false);
        $("#togBtn2").prop("checked", false);
        $("#togBtn3").prop("checked", false);
        $(".existing-metadata-tags").html('');
        $(".image-info").hide();
        $("#tagged").css("display", "none");
        $(".click_on_image").css("display", "none");

        $("#Metadata").css("display", "none");
        $("#Metadata2").css("display", "none");
        $(".table-body").html("");

        $('#save_roi').show();
        $('#cancel_roi').show();

        current_img = document.getElementById('expandedImg');
        drawRectangleROI(current_img);
    });

    $('#cancel_roi').click(function (e) {
        e.preventDefault();
        if (cropper != null) {
            cropper.destroy();
            cropper = null;
        }
        if (document.getElementById('image_canvas') != null) {
            document.getElementById('image_canvas').remove();
        }
        $('#cancel_roi').hide();
        $('#save_roi').hide();
    });

    /**
     *
     * Event when clicked on the toggle button for showing ROIs
     */
    var switchStatus = false;
    $('#togBtn4').on('change', function (e) {
        if ($(this).is(':checked')) {
            switchStatus = $(this).is(':checked');

            original_image = image_src.split(prefix)[1];
            file_name = image_src.substring(image_src.lastIndexOf("/") + 1).split(".")[0];

            current_img = document.getElementById('expandedImg');

            getROIdata();

            $("#keyValueDiv").hide();
            $("#togBtn3").prop('checked', false);
            $(".keyvalue-table-body").html("");
            $(".table-body").html("");
            $(".roi-table-body").html("");

            $("#keyvalue-info").hide();
            $(".error-msg").hide();
            $(".success-msg").hide();
            $(".keyValue").hide();
            $("#togBtn2").prop('checked', false);
            $(".image-info").hide();



        }
        else {
            if (document.getElementById('image_canvas') != null) {
                document.getElementById('image_canvas').remove();
            }
            switchStatus = $(this).is(':checked');
            $('.roi-table-body').html("");
            $('.roidata-info').hide();
        }
    });

    /**
     * Function that parses the information of the selected ROI in the table, to draw
     * the ROI on the image.
     */
    $(".roi-table-body").mouseup(function (e) {
        e.preventDefault();
        for (var i = 0; i < $(".roi-table-body > div").length; i++) {
            if(!$("#row" + i.toString()).is(e.target) && $("#row" + i.toString()).has(e.target).length === 0) {
                // Do nothing
            } else {
                var x = $("#row" + i.toString()).children('#X').text();
                var y = $("#row" + i.toString()).children('#Y').text();
                var width = $("#row" + i.toString()).children('#WIDTH').text();
                var height = $("#row" + i.toString()).children('#HEIGHT').text();

                createCanvas(x, y, width, height);
            }
        }
    });


    /**
     * Event for saving the selected ROI
     */
    $('#save_roi').click(function (e) {
        e.preventDefault();

        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");

        if (cropper) {
            var canvas = cropper.getCroppedCanvas({
                imageSmoothingQuality: 'high'
            });

            var ROIdata = [cropper.getData().x, cropper.getData().width,
                cropper.getData().y, cropper.getData().height];

            original_image = image_src.split(prefix)[1];
            file_name = image_src.substring(image_src.lastIndexOf("/") + 1).split(".")[0];

            // Submit the created ROI to the server
            canvas.toBlob(function (blob) {
                var form_data = new FormData();

                // Form data for creating new image from ROI
                form_data.append('selectedROI', blob);
                form_data.append('originalImage', original_image);
                form_data.append('imageName', file_name);
                $.ajax({
                    url: '/ROIcreation',
                    beforeSend: function (request) {
                        request.setRequestHeader(header, token);
                    },
                    data: form_data,
                    processData: false,
                    contentType: false,
                    type: 'POST'
                }).done(function (data) {
                    saveRoiCoords(ROIdata, header, token);
                    getUpdatedData(current_folder, current_direction, current_history,
                        current_main_folder, current_main_data);
                    $(".div_right").append(
                        '<div id="success-msg-roi" class="success-msg">' +
                        '<i class="fa fa-check"></i>' +
                        ' Successfully created ROI for image ' + image_src.substring(image_src.lastIndexOf("/") + 1) +
                        '</div>');
                    $(".success-msg").delay(400).fadeIn();
                    $(".success-msg").delay(5000).fadeOut();
                }).fail(function (data) {
                    $(".div_right").append(
                        '<div id="error-msg-roi" class="error-msg"> ' +
                        '<i class="fa fa-times-circle"></i> Failed to create ROI because of an internal sever error for image' + image_src.substring(image_src.lastIndexOf("/") + 1) + '</div>');
                    $(".error-msg").delay(400).fadeIn();
                    $(".error-msg").delay(5000).fadeOut();
                });
            });
        }
    });

    /**
     * Hide the ROI functionality when clicking somewhere else on the screen
     */
    $(document).mouseup(function (e) {
        var expandedImg = $('#expandedImg');
        var saveButton = $('#save_roi');
        if (!expandedImg.is(e.target) && expandedImg.has(e.target).length === 0 &&
            !saveButton.is(e.target) && saveButton.has(e.target).length === 0) {
            if (cropper != null) {
                cropper.destroy();
                cropper = null;
            }
            $('#cancel_roi').hide();
            $('#save_roi').hide();
        }
    });

});

/**
 * Function that creates a canvas and draws the selected ROI on the image
 * @param x
 * @param y
 * @param width
 * @param height
 */
function createCanvas(x, y, width, height) {
    if (document.getElementById('image_canvas') != null) {
        document.getElementById('image_canvas').remove();
    }
    canv_width = current_img.clientWidth;
    canv_height = current_img.clientHeight;

    image_canvas = document.createElement('div');
    container = document.getElementById('container');
    image_canvas.id = "image_canvas";
    image_canvas.class = "image_canvas";
    image_canvas.height = canv_height;
    image_canvas.width = canv_width;

    container.appendChild(image_canvas);

    // Create Raphael objects which draws the rectangle ROI on the image
    var paper = new Raphael(document.getElementById("image_canvas"), canv_width, canv_height);
    paper.rect(x, y, width, height)
        .attr({
            stroke: "#000",
            "stroke-width": 3
        });
}

/**
 * Function that creates the rectangle cropper object.
 * @param image_source
 */
function drawRectangleROI(image_source) {
    var options = {
        scalable: true,
        zoomable: true,
        rotatable: true,
        checkOrientation: true,
        crop: function (event) {
            console.log(event.detail.x);
            console.log(event.detail.y);
            console.log(event.detail.width);
            console.log(event.detail.height);
        }
    };
    cropper = new Cropper(image_source, options);
}

/**
 * Function that fills the ROI table of the selected image with the information per ROI
 */
function getROItable() {
    for (var i = 0; i < ROIs.length; i++) {
        $('.roi-table-body').append(
            '<div id="row' + i.toString() + '" class="table-body-row">' +
                '<div class="source-cell">' + file_name + '</div>' +
                '<div class="source-cell" id="X">' + ROIs[i].x + '</div>' +
                '<div class="source-cell" id="Y">' + ROIs[i].y + '</div>' +
                '<div class="source-cell" id="WIDTH">' + ROIs[i].width + '</div>' +
                '<div class="source-cell" id="HEIGHT">' + ROIs[i].height + '</div>' +
            '</div>'
        );
        $(".roidata-info").show();
    }
}

/**
 * Function that gets the required ROI and its data from the selected image.
 */
function getROIdata() {
    var image_source_edit = original_image.replace(/[/\\\\]+/g, "+");
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "/showROIs/" + image_source_edit,
        success: function (data) {
            ROIs = [];
            for (var i = 0; i < data.length; i++) {
                var roi = {x: data[i].xcoord,
                           width: data[i].width,
                           y: data[i].ycoord,
                           height: data[i].height};
                ROIs.push(roi);
            }
            console.log(ROIs);
            getROItable();
        }
    });
}

/**
 * Function that saves the coordinates of a specific ROI in the database
 * @param ROIdata
 * @param xhr_coords
 * @param header
 * @param token
 */
function saveRoiCoords(ROIdata, header, token) {
    var ROI_data = new FormData();
    // Form data for saving coordinates from ROI
    ROI_data.append('ROIdata', ROIdata);
    ROI_data.append('imageSource', original_image);

    $.ajax({
        url: '/storeRoiCoordinates',
        beforeSend: function (request) {
            request.setRequestHeader(header, token);
        },
        data: ROI_data,
        processData: false,
        contentType: false,
        type: 'POST'
    }).done(function (data) {
        console.log("Successfully stored ROI coordinates!")
    }).fail(function (data) {
        console.log("Storing of ROI coordinates FAILED!")

    });
}