/**
 * JS file that creates the functionalities when an image is being clicked
 *
 * @author
 * initialized by Kylie
 * edited by Willemijn
*/

var thumb_img_src;
var image_name;
var thumbnailLength;
var extensionStart;
var image_extension;
var image_src;
var expandImg;
var prefix;

var canv_height;
var canv_width;

$(document).ready(function () {
    var vheight = $(window).height();
    var globalTimer = null;
    $(window).resize(function () {
        clearTimeout(globalTimer);
        globalTimer = setTimeout(doneResize, 750);
    });

    function doneResize() {
        if ($("#togBtn").prop("checked") === true) {

            if ($(this).height() != vheight) {
                vheight = $(this).height();
                $("#togBtn").prop("checked", false);
                $(".existing-metadata-tags").html("");
                alert("You changed the screen resolution; re-toggle the 'show metadata on image' " +
                "button to correctly position the image tags");

                return false;
            }

        }
        return false;
    }

    prefix = window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/';

    var tagged = document.getElementById("#tagged");

    /**
     * This function is called when the close button is clicked.
     * Everything in the "editor" window will be removed.
     */
    document.getElementById("closebtn").onclick = function () {

        if (document.getElementById('image_canvas') != null) {
            document.getElementById('image_canvas').remove();
        }

        var image = document.getElementById("expandedImg");
        var placeholder = document.getElementById("div_right_placeholder");
        placeholder.style.display = "block";
        placeholder.style.right = 'auto';
        image.parentElement.style.display = "none";
        $(tagged).hide();

        $(".image-info").hide();
        $(".table-body").html("");
        $("#keyvalue-info").hide();
        $(".keyvalue-table-body").html("");
        $(".switch").hide();
        $(".switch2").hide();
        $(".switch3").hide();
        $(".keyValue").hide();
        $("#keyValueDiv").hide();
        $(".existingMeta").hide();

        $(".options_buttons").hide();
        // $("#create_xytag").hide();
        // $("#create_generaltag").hide();
        $(".click_on_image").hide();
        $(".metadata-input").hide();
        $("#togBtn2").hide();
        $("#togBtn3").hide();
        $("#togBtn4").hide();
    };


    /**
     * This function registers clicks on the expanded image
     * which is positioned in the editor window
     */
    document.getElementById("expandedImg").onclick = function (event) {

        if (document.getElementById('image_canvas') != null) {
            document.getElementById('image_canvas').remove();
        }

        if (document.getElementById('image_canvas') != null) {
            document.getElementById('image_canvas').remove();
        }

        var image = document.getElementById("expandedImg");
        var tag = document.getElementById("tagged");

        var pos_x = (event.offsetX ? (event.offsetX) : event.pageX - image.offsetLeft);
        var pos_y = (event.offsetY ? (event.offsetY) : event.pageY - image.offsetTop);

        tag.style.left = (pos_x - 8).toString() + 'px';
        tag.style.top = (pos_y - 8).toString() + 'px';

        var display = $(".click_on_image").css("display");
        if (display === "block") {
            $("#Metadata").show();
        }
    };

    /**
     * When clicked on keyValue, the input form appears for adding key-value pairs
     * Function which views the input form for adding key-value pairs to images
     */
    document.getElementById("keyValue").onclick = function showKeyValue() {
        console.log("inside show key value function");

        $("#keyValueDiv").show();
        // hides
        $(".image-info").hide();
        $("#Metadata2").hide();
        $("#Metadata").hide();
        $(".click_on_image").hide();
        $("#tagged").hide();
        $("#togBtn2").prop("checked", false);
        $("#togBtn3").prop("checked", false);
        $("#keyvalue-info").hide();

        /**
         * Function which dynamically adds a new key-value
         * field to the form when "add field" is clicked.
         */
        document.getElementById("add_key_value").onclick = function (e) {
            $("#submit_key_value").attr("disabled", false);
            $("#submit_key_value").css("background-color", "grey");
            e.preventDefault();
            $("#submit_key_value").before('<div class="keyvaldiv"><input type="text" id="added_key" name="added_key" class="added_key" placeholder="key" required>' +
                '<label class="input-remove-button">x</label>' +
                '<input type="text" id="added_value" name="added_value" class="added_value" placeholder="value" required></div>');
            $(".added_key").css("width", "115px");
            $(".added_key").css("float", "left");
            $(".added_value").css("width", "200px");
            $(".added_value").css("float", "right");
        };

    };

    /**
     * Function which checks the input lengths
     * when there is ANY change in the key-value form
     */
    $("#keyValueForm").change(checkInput);
    function checkInput() {
        var fields$ = $(".added_value, .added_key");
        var count = 0;
        fields$.each(function (e) {
            if($(this).val().length !== 0 ) {
                count += 1;
            }
        });
        if(count === ($('.added_value').length + $('.added_key').length )) {
            $("#submit_key_value").attr("disabled", false);
            $("#submit_key_value").css("background-color", "#39ae3f");
        } else {
            $("#submit_key_value").attr("disabled", true);
            $("#submit_key_value").css("background-color", "grey");
        }
    }

    /**
     * General function for sending a form with data
     * to the back-end.
     * @param name              (what request is it, used for additional actions)
     * @param url               (url mapping for controller)
     * @param formData          (the data which the backend needs)
     * @param succes_message    (success message to be displayed if needed)
     * @param error_message     (success message to be displayed if needed)
     * @param e                 (event)
     */
    function performPost(name, url, formData, succes_message, error_message, e) {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $.ajax({
            url: url,
            beforeSend: function(request) {
                request.setRequestHeader(header, token);
            },
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST'
        }).done(function( data ) {
            if (name === "validate") {
                $(e.target).parent().html('<span class= "validate-yes" style="color: green;">true</span>');
            } else if (name === "remove_row") {
                $(e.target).parent().parent().remove();
            }
            $(".div_right").append(
                '<div id="success-msg-keyval" class="success-msg">' +
                '<i class="fa fa-check"></i>&nbsp;' +
                ''+ succes_message +'' +
                '</div>');
            $(".success-msg").delay(400).fadeIn();
            $(".success-msg").delay(5000).fadeOut();
        }).fail(function( data ) {
            $(".div_right").append(
                '<div id="error-msg-keyval" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;' +
                ''+ error_message +'' +
                '</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        });
    }

    /**
     * Function which initiates the storage of the filled out
     * key-value form by calling saveFormData function
     */
    $("#submit_key_value").click (function(e) {
        e.preventDefault();
        // FILL DATA OBJECT
        var image_source = document.getElementById("expandedImg").src;
        var image_name = image_source.substring(image_source.lastIndexOf('/') + 1);
        var form = document.getElementById("keyValueForm");
        var formData = new FormData(form);
        formData.append("image_source", image_name);

        // VARIABLES FOR AJAX REQUEST
        var url = '/KeyValue';
        var succes_message = "You successfully stored the key-value pair(s)!";
        var error_message = "Error: something went wrong while trying to store your key-value pair(s)";

        // SEND TO BACKEND / SAVE THE FORM DATA
        performPost(url, formData, succes_message, error_message);

        // DISPLAYING LOGIC
        $('.error-msg').remove();
        $('.success-msg').remove();
        form.reset();
        $("#submit_key_value").attr("disabled", false);
        $("#submit_key_value").css("background-color", "grey");
        $("#keyValueDiv").hide();
        resetKeyValueForm();
    });

    /**
     * Function which resets the key-value form
     * to the four standard fields
     */
    function resetKeyValueForm() {
        $(".keyvalueform").html("");
        $(".keyvalueform").append(
            '<div class="keyvaldiv">' +
            '<input type="text"  name="added_key" class="added_key" placeholder="pH" required>' +
            '<label class="removebutton">x</label>' +
            '<input type="text" id="added_value" name="added_value" class="added_value" placeholder="value"required >' +
            '</div>' +
            '<div class="keyvaldiv">' +
            '<input type="text" name="added_key" class="added_key" placeholder="Oxygen" required>' +
            '<label class="removebutton">x</label>' +
            '<input type="text" name="added_value" class="added_value" placeholder="value" required>' +
            '</div>' +
            '<div class="keyvaldiv">' +
            '<input type="text"name="added_key" class="added_key" placeholder="CO2" required>'+
            '<label class="removebutton">x</label>' +
            '<input type="text" name="added_value" class="added_value" placeholder="value" required>' +
            '</div>' +
            '<div class="keyvaldiv">' +
            '<input type="text"name="added_key" class="added_key" placeholder="Temperature" required>' +
            '<label class="removebutton">x</label>' +
            '<input type="text" name="added_value" class="added_value" placeholder="value"required>' +
            '</div>' +
            '<button id="submit_key_value" class="key-value-btn" type="submit" disabled>Submit</button>' +
            '<button id="add_key_value" class="key-value-btn">Add new field</button>'
        );
    }

    /**
     * Function which gets the key-value pairs of an image and
     * visualizes it within a table
     */
     function getKeyvaluePairs() {
        $("#Metadata2").hide();
        $("#Metadata").hide();
        $(".click_on_image").hide();

        var image_source = document.getElementById("expandedImg").src;
        var image_name = image_source.substring(image_source.lastIndexOf('/') + 1);
        console.log("source" + image_source);
        console.log("image name of which the key-value pairs are retrieved = " + image_name);
        $.ajax({
            type: "GET",
            contentType: "application/json",
            dataType: "json",
            url: "/KeyValue/" + image_name,
            success: function (data) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    if (parseInt(data[i].validated) === 1) {
                        $(".keyvalue-table-body").append(
                            '<div id="row' + i.toString() + '" class="keyvalue-table-body-row">' +
                            '<div class="key-cell">' + data[i].tag_key + '</div>' +
                            '<div class="value-cell">' + data[i].tag_value + '</div>' +
                            '<div class="creator-keyvalue-cell">' + data[i].creator + '</div>' +
                            '<div class="source-cell">' + data[i].image_source + '</div>' +
                            '<div class="date-keyvalue-cell">' + data[i].date + '</div>' +
                            '<div class="validated-cell"><span class="validate-yes">true</span></div>' +
                            '</div>'
                        );
                    } else {
                        $(".keyvalue-table-body").append(
                            '<div id="row' + i.toString() + '" class="keyvalue-table-body-row">' +
                            '<div class="key-cell">' + data[i].tag_key + '</div>' +
                            '<div class="value-cell">' + data[i].tag_value + '</div>' +
                            '<div class="creator-keyvalue-cell">' + data[i].creator + '</div>' +
                            '<div class="source-cell">' + data[i].image_source + '</div>' +
                            '<div class="date-keyvalue-cell">' + data[i].date + '</div>' +
                            '<div class="validated-cell"><span class="validate-no">false</span>&nbsp;&nbsp;<span id="validate-button' + i + '" class="validate-button">[validate]</span></div>' +
                            '</div>'
                        );
                    }

                    $(".validate-no").css("color", "red");
                    $(".validate-yes").css("color", "green");
                    // After filling in the table with the information the table is viewed
                    $("#keyvalue-info").show();
                }
            },
            error: function (xhr, status) {
                console.log("inside error");
                getErrorMessage(xhr, status, "get key-value pairs");
            }
        });
     }




    /**
     * Create the functionality for renaming an image
      */
    $('#img_option_rename').click(function (e) {
        e.preventDefault();

        // Create placeholder
        var placeholder = image_src.split(/[/\\]+/).pop();
        if (placeholder.length > 18) {
            var part1 = placeholder.substring(0,8);
            var part2 = placeholder.substring(placeholder.length - 8, image_src.length);
            placeholder = part1 + '..' + part2;
        }
        $('#new_img_name').attr('placeholder', placeholder);
        $('#rename_img').show();
    });
    $('#img_rename_form').submit(function (e) {
        e.preventDefault();
        var new_image_name = $('[name="new_img_name"]').val();
        if (isValidName(new_image_name)){
            renameImage(image_src, thumb_img_src, prefix, new_image_name);
            calculateDivHeights();
        } else {
            var fail_rename = document.getElementById("fail_rename");
            fail_rename.innerHTML = "New image name '" + new_image_name + "' contains invalid character(s)";
            $('#img_rename_form')[0].reset();
            $(fail_rename).delay(500).fadeIn();
            $(fail_rename).delay(5000).fadeOut();
        }
        $('#rename_img').hide();
        $('#dir_options').show();
    });

    /**
     * Create the functionality for deleting an image
     */
    $('#img_option_delete').click(function (e) {
        e.preventDefault();
        if (confirm("Are you sure you want to delete image '" + image_src.split(prefix)[1] + "'?")) {
           deleteImage(image_src, thumb_img_src, prefix);
           calculateDivHeights();
       }
    });

    /**
     * This piece of code is the functionality behind the
     * show metadata (on image) toggle button
     */
    var switchStatus = false;
    $("#togBtn").on('change', function() {
        if ($(this).is(':checked')) {
            switchStatus = $(this).is(':checked');

            $("#togBtn4").prop('checked', false);
            $('.roi-table-body').html("");
            $('.roidata-info').hide();

            getMetadataOnImage();
        }
        else {
            switchStatus = $(this).is(':checked');
            $(".existing-metadata-tags").html("");
        }
    });

    /**
     * @function show / hide metadata table
     */
    var switchStatus2 = false;
    $("#togBtn2").on('change', function() {
        if ($(this).is(':checked')) {
            console.log("table button is used");
            switchStatus2 = $(this).is(':checked');
            // Make sure the table for the key-value pairs is hidden when the metadata table needs to be showed
            $("#keyValueDiv").hide();
            $("#togBtn3").prop('checked', false);
            $("#togBtn4").prop('checked', false);
            $(".keyvalue-table-body").html("");
            $("#keyvalue-info").hide();
            $(".error-msg").hide();
            $(".success-msg").hide();
            $(".keyValue").hide();
            $('.roi-table-body').html("");
            $('.roidata-info').hide();
            getMetadataForTable();
        }
        else {
            switchStatus2 = $(this).is(':checked');
            $(".table-body").html("");
            $(".image-info").hide()
        }
    });

    /**
     * Function which visualizes the key-value pairs in table
     */
    var switchStatus3 = false;
    $("#togBtn3").on('change', function() {
        if ($(this).is(':checked')) {
            switchStatus3 = $(this).is(':checked');
            // Make sure the table for the metadata is hidden when the table for the key-value pairs needs to be showed
            $("#keyValueDiv").hide();
            $("#togBtn2").prop('checked', false);
            $("#togBtn4").prop('checked', false);
            $(".table-body").html("");
            $(".image-info").hide();
            $(".keyValue").hide();
            $('.roi-table-body').html("");
            $('.roidata-info').hide();
            getKeyvaluePairs();
        }
        else {
            switchStatus2 = $(this).is(':checked');
            $(".keyvalue-table-body").html("");
            $("#keyvalue-info").hide()
        }
    });

    /**
     * @function initiation of validating a tag
     *
     * When clicking on the table body and the validate button
     * is the target, then open a POST to validate the tag when
     * OK is pressed in the confirm alert.
     */
    $(".table-body").mouseup(function (e) {
        console.log("aantal rijen " + $(".table-body > div").length);
        if($(e.target).is(".validate-button")) {
            e.preventDefault();
            if(confirm("Are you sure you want to validate this tag? \nNOTE: This cannot be undone!")) {
                var image_source = document.getElementById("expandedImg").src;
                var image_name = image_source.substring(image_source.lastIndexOf('/') + 1);
                console.log("source" + image_source);
                document.getElementById("validate-button");
                var row_element = $(e.target).parent().parent();
                var date_element = $(row_element).find('.date-cell');
                var date_of_creation = $(date_element).html();

                var tag_element = $(row_element).find('.tag-cell-inp');
                var tag_name = $(tag_element).html();

                // creating data object for back-end
                var data = new FormData();
                // data.append("validated", true);
                // data.append("date_of_creation",  date_of_creation);
                // data.append("tag_name",  tag_name);
                var url = '/api/metadata/v1/validate/tag?validated=true&date_of_creation='+ date_of_creation +'&tag_name='+ tag_name +'&image_source=' + image_name;
                var name = "validate";
                var succes_message = 'You succesfully validated the tag';
                var error_message = 'Error: something went wrong while trying to validate this key-value pair';

                performPost(name, url, data, succes_message, error_message, e);

            }
            $(".error-msg").remove();
            $(".success-msg").remove();
        }

        /**
         * @function Highlight tags positioned on image
         *
         * For every row in table body when clicked
         * highlight the row by making it light grey and
         * highlight the tag by making it blue
         */
        for (var i = 0; i < $(".table-body > div").length; i++){
            if(!$("#row" + i.toString()).is(e.target) && $("#row" + i.toString()).has(e.target).length === 0) {
                $("#row" + i.toString()).css("background", "white");
                $("#existMeta" + i.toString()).css("background-color", "initial");
            } else {
                $("#row" + i.toString()).css("background-color", "#dfe6e9");
                if ($("#togBtn").prop("checked") === true) {
                    $("#existMeta" + i.toString()).css("background-color", "dodgerblue");
                }
            }
        }

    });

    /**
     * @function initiation of validating a key-value pair
     *
     * When clicking on the table body and the validate button
     * is the target, then open a POST to validate the key-value
     * pair when OK is pressed in the confirm alert.
     */
    $(".keyvalue-table-body").mouseup(function (e) {
        console.log("aantal rijen " + $(".keyvalue-table-body > div").length);
        if($(e.target).is(".validate-button")) {
            e.preventDefault();
            if(confirm("Are you sure you want to validate this key-value pair? \nNOTE: This cannot be undone!")) {
                var image_source = document.getElementById("expandedImg").src;
                var image_name = image_source.substring(image_source.lastIndexOf('/') + 1);
                console.log("source" + image_source);
                document.getElementById("validate-button");
                console.log("image_name = " + image_name);

                var row_element = $(e.target).parent().parent();
                var date_element = $(row_element).find('.date-keyvalue-cell');
                var date_of_creation = $(date_element).html();
                var tag_element = $(row_element).find('.value-cell');
                var value_cell = $(tag_element).html();
                var data = new FormData();
                data.append("validated", true);
                data.append("value", value_cell);
                data.append("date", date_of_creation);
                var name = "validate";
                var succes_message = 'You successfully validated the key-value pair(s)';
                var error_message = 'Error: something went wrong while trying to validate this key-value pair';
                var url = '/validatekeyvalue/' + image_name;

                performPost(name, url, data, succes_message, error_message, e);

            }
            $(".error-msg").remove();
            $(".success-msg").remove();
        }
    });

    /**
     * @function remove input row in key-value form
     */
    $(".keyvalueform").on('click', '.input-remove-button', function(e){
        e.preventDefault();
        $(this).parent('div').remove();
        //check if input is correct
        checkInput()
    });


    /**
     * Function which removes database instance on click + confirm
     */
    // TODO: MAKE ONLY VISIBLE FOR ADMIN (Must be done via sending html back from the controller)
    $(".table-body").mouseup(function (e) {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        if ($(e.target).is(".metadata-remove-row i")) {
            e.preventDefault();
            if (confirm("Are you sure you want to permanently remove this tag from the database? \nNOTE: This cannot be undone!")) {
                var image_source = document.getElementById("expandedImg").src;
                var image_name = image_source.substring(image_source.lastIndexOf('/') + 1);

                var row_element = $(e.target).parent().parent();
                var date_element = $(row_element).find('.date-cell');
                var date_of_creation = $(date_element).html();

                var tag_element = $(row_element).find('.tag-cell-inp');
                var tag_name = $(tag_element).html();

                // creation of data object for backend
                var data = new FormData();
                data.append("date_of_creation", date_of_creation);
                data.append("tag_name", tag_name);
                var url = '/removemetadata/' + image_name;
                var name = "remove_row";
                var succes_message = 'Successfully deleted tag: ' + tag_name;
                var error_message = 'Error: something went wrong while trying to delete this metadata tag';

                // PERFORM AJAX POST / SEND FORM DATA TO BACKEND
                performPost(name, url, data, succes_message, error_message, e);

            }
            $(".error-msg").remove();
            $(".success-msg").remove();
        }
    });
});

/**
 * This function is called when an image in the image browser
 * is clicked.
 * It opens the full res image in the editor portion of the application.
 */
function expandClickedImage(imgs) {
    // HIDE / SHOW / ALTERATIONS
    $(".image-info").hide();
    $(".table-body").html("");
    $("#keyValueDiv").hide();

    $('.roidata-info').hide();
    $('.roi-table-body').html("");

    $("#keyvalue-info").hide();
    $(".keyvalue-table-body").html("");
    $(".keyValue").hide();


    $("#togBtn").prop('checked', false);
    $("#togBtn2").prop('checked', false);
    $("#togBtn3").prop('checked', false);
    $("#togBtn4").prop('checked', false);
    $(".switch2").show();
    $(".switch").show();
    $(".switch3").show();

    $(".click_on_image").hide();
    $("#tagged").hide();
    $("#tagged").css("left", "0");
    $("#tagged").css("top", "0");
    $(".existingMeta").hide();
    $(".existing-metadata-tags").html("");
    $(".container").show();

    if (document.getElementById('image_canvas') != null) {
        document.getElementById('image_canvas').remove();
    }

    // The thumbnail image source
    thumb_img_src = imgs.src;
    console.log("Thumbnail image source = " + imgs.src);

    // Get the expanded image
    console.log(document);
    expandImg = document.getElementById("expandedImg");
    var placeholder = document.getElementById("div_right_placeholder");

    // Get the image text
    var imgText = document.getElementById("imgtext");

    // Get original image name of thumbnail image
    image_name = imgs.src.substr(0, imgs.src.indexOf("_thumb"));
    console.log("this is image name", image_name);
    thumbnailLength = imgs.src.length;
    extensionStart = imgs.src.indexOf(".");
    image_extension = imgs.src.substr(extensionStart, thumbnailLength);
    // The imagename without the _thumb
    image_src = image_name.concat(image_extension);
    console.log("image source file (original) viewed as big image when clicked on = " + image_src);

    // Use the original high quality image source in the expanded image as the image being clicked on from the grid
    expandImg.src = image_src;

    // Use the value of the alt attribute of the clickable image as text inside the expanded image
    imgText.innerHTML = image_src.substring(image_src.lastIndexOf("/") + 1, image_src.length);
    //var total_width = document.getElementById("container").clientWidth;
    var width = document.getElementById("expandedImg").clientWidth;
    console.log(width);

    //var close = document.getElementById("closebtn");

    // SHOW: image + option buttons + image delete/rename options
    // HIDE: metadata input forms + dir delete/rename options + placeholder text
    $(placeholder).hide();
    $(".options_buttons").show();
    $(".metadata-input").hide();
    $('#img_options').show();
    $('#dir_options').hide();

    original_image = image_src.split(prefix)[1];
}

/**
 * This function is called when the "show metadata" (on image ) toggle
 * is being activated (checked)
 * It gets the metadata from the backend (database) and shows it on
 * the image that is currently displayed
 */
function getMetadataOnImage() {
    var image_source = document.getElementById("expandedImg").src;
    var image_name = image_source.substring(image_source.lastIndexOf('/') + 1);
    console.log("source" + image_source);
    console.log("image_name = " + image_name);
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "/showmetadata/" + image_name
    }).done(function ( data ) {
        $(".existing-metadata-tags").html("");
        for (var i = 0; i < data.length; i++) {
            if (data[i].xcoord !== 0.0) {
                $('.existing-metadata-tags').append('<div class="existingMeta" id="existMeta' + i + '"></div>');
                $("#existMeta" + i.toString()).append('<span class="tag_span_name" id="tag_name_span' + i + '">' + data[i].tag + '</span>');
                var xcoord_px = (data[i].xcoord * $(".expandedImg").width());
                var ycoord_px = (data[i].ycoord * $(".expandedImg").height());

                var xcoord = xcoord_px.toString() + 'px';
                var ycoord = ycoord_px.toString() + 'px';

                var idNameMetadata = 'existMeta'.concat(i.toString());
                var metadata = document.getElementById(idNameMetadata);
                $('.existingMeta').show();
                $(metadata).css("left", xcoord);
                $(metadata).css("top", ycoord);
                $('#tag_name_span' + i.toString()).css("position", "absolute");
                $('#tag_name_span' + i.toString()).css("left", "8px");
                $('#tag_name_span' + i.toString()).css("top", "14px");

                $(".existingMeta").show();
            }
        }
    }).fail(function ( data ) {
        $(".div_right").append(
            '<div id="error-msg-valid" class="error-msg"> ' +
            '<i class="fa fa-times-circle"></i>&nbsp;No metadata available' +
            '</div>');
        $(".error-msg").delay(400).fadeIn();
        $(".error-msg").delay(5000).fadeOut();
    });
}


/**
 * This function is called when the "show metadata table" toggle
 * has been activated (checked).
 * It gets all the matadata and forms a table.
 */
function getMetadataForTable() {
    $("#tagged").hide();
    $("#tagged").css("left", "0");
    $("#tagged").css("top", "0");
    $("#Metadata2").hide();
    $("#Metadata").hide();
    $(".click_on_image").hide();
    console.log("inside function");
    var image_source = document.getElementById("expandedImg").src;
    var image_name = image_source.substring(image_source.lastIndexOf('/') + 1);

    // GET VIA API (REST-CONTROLLER)
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "/api/metadata/v1/get/by/source?source="+image_name+"&",
        success: function (data) {
            if (data.length === 0) {
                console.log("geen meta");
            }
            var data2 = data["body"];
            for (var i = 0; i < data2.length; i++) {
                if (data2[i].xcoord === 0.0 && data2[i].ycoord === 0.0) {
                    var type = "general";
                } else {
                    var type = "positional";
                }
                console.log(data2[i].validated);
                if (data2[i].validated === true) {
                    $(".table-body").append(
                        '<div id="row' + i.toString() + '" class="table-body-row">' +
                            '<div class="type-cell-inp">' + type + '</div>' +
                            '<div class="tag-cell-inp">' + data2[i].tag + '</div>' +
                            '<div id="creator-cell" class="creator-cell-inp">' + data2[i].creator + '</div>' +
                            '<div class="source-cell">' + data2[i].source + '</div>' +
                            '<div class="date-cell">' + data2[i].date_of_creation + '</div>' +
                            '<div class="validated-cell-inp"><span class="validate-yes">true</span></div>' +
                            '<label class="metadata-remove-row"><i class="far fa-times-circle"></i></label>' +
                        '</div>'
                    );
                } else {
                    $(".table-body").append(
                        '<div id="row' + i.toString() + '" class="table-body-row">' +
                            '<div class="type-cell-inp">' + type + '</div>' +
                            '<div class="tag-cell-inp">' + data2[i].tag + '</div>' +
                            '<div id="creator-cell" class="creator-cell-inp">' + data2[i].creator + '</div>' +
                            '<div class="source-cell">' + data2[i].source + '</div>' +
                            '<div class="date-cell">' + data2[i].date_of_creation + '</div>' +
                            '<div class="validated-cell-inp"><span class="validate-no">false</span>&nbsp;&nbsp;<span id="validate-button' + i + '" class="validate-button">[validate]</span></div>' +
                            '<label class="metadata-remove-row"><i class="far fa-times-circle"></i></label>' +
                        '</div>'
                    );

                }


                $(".validate-no").css("color", "red");
                $(".validate-yes").css("color", "green");
                $(".image-info").show();
                if ($(".hello_message").html().includes("USER")) {
                    $(".metadata-remove-row").hide()
                }

            }
        },
        error: function (xhr, status) {
        console.log("inside error");
        getErrorMessage(xhr, status, "get metadata on image");
    }
    });


    /**
     * Search functionality for the metadata table, to filter per creator
     */
    $("#searchBar").keyup(function() {
        console.log("in search bar function of creator");

        var searchInput = document.getElementById("searchBar");
        var filter = searchInput.value.toUpperCase();
        var creator = document.getElementsByClassName("creator-cell-inp");
        //console.log("creator leng", creator.length);

        for (var i = 0; i < creator.length; i++) {
            var value = creator[i].firstChild;
            if (value) {
                var result = value.textContent || value.innerText;
                if (result.toUpperCase().indexOf(filter) > -1) {
                    console.log("row"+i);
                    document.getElementById("row"+i).style.display = "";
                } else {
                    document.getElementById("row"+i).style.display = "none";
                }
            }
        }
    });

    /**
     * Search functionality for the metadata table, to filter per tag name
     */
    $("#searchBarTag").keyup(function() {
        console.log("in search bar function of tag name");

        var searchInput = document.getElementById("searchBarTag");
        var filter = searchInput.value.toUpperCase();
        var tags = document.getElementsByClassName("tag-cell-inp");
        //console.log("creator leng", creator.length);
        console.log(filter);
        for (var i = 0; i < tags.length; i++) {
            var value = tags[i].firstChild;
            if (value) {
                var result = value.textContent || value.innerText;
                if (result.toUpperCase().indexOf(filter) > -1) {
                    document.getElementById("row"+i).style.display = "";
                } else {
                    document.getElementById("row"+i).style.display = "none";
                }
            }
        }
    });

    /**
     * Search functionality for the metadata table, to filter per tag type
     */
    $("#searchBarType").change(function() {
        console.log("in search bar function of tag type");

        var searchInput = document.getElementById("searchBarType");
        var filter = searchInput.value.toUpperCase();
        var type = document.getElementsByClassName("type-cell-inp");
        //console.log("creator leng", creator.length);
        console.log(searchInput);
        for (var i = 0; i < type.length; i++) {
            var value = type[i].firstChild;
            if (value) {
                var result = value.textContent || value.innerText;
                if (result.toUpperCase().indexOf(filter) > -1) {
                    console.log("row"+i);
                    document.getElementById("row"+i).style.display = "";
                } else {
                    document.getElementById("row"+i).style.display = "none";
                }
            }
        }
    });

    /**
     * Search functionality for the metadata table, to filter per tag type
     */
    $("#drop-btn").click(function() {
        console.log("in search bar function of tag type");
        //document.getElementById("drop-opt").classList.toggle("show");
        $("#drop-opt").show();

        var type = document.getElementsByClassName("type-cell-inp")
        // If positional is asked to show
        $("#pos").click(function() {
            for (var i=0; i < type.length; i++) {
                var value = type[i].firstChild;
                if (value) {
                    var result = value.textContent || value.innerText;
                    if (result.indexOf("positional") > -1) {
                       //console.log("row"+i);
                      document.getElementById("row"+i).style.display = "";
                    } else {
                     document.getElementById("row"+i).style.display = "none";
                      }
                }
            }
        });
        // if general is asked to show
        $("#gen").click(function () {
            for (var i=0; i < type.length; i++) {
                var value = type[i].firstChild;
                if (value) {
                    var result = value.textContent || value.innerText;
                    if (result.indexOf("general") > -1) {
                        //console.log("row"+i);
                        document.getElementById("row"+i).style.display = "";
                    } else {
                        document.getElementById("row"+i).style.display = "none";
                    }
                }
            }
        });

        // if all is asked to show
        $("#all").click(function () {
            for (var i=0; i < type.length; i++) {
                document.getElementById("row"+i).style.display = "";
            }
        });

        // hide dropdown options
        window.onclick = function(event) {
            if (!event.target.matches('#drop-btn')) {
                $("#drop-opt").hide();
            }
        }
    });

    /**
     * Search functionality for the metadata table, to filter whether tag is validated or not
     */
    $("#drop-valid-btn").click(function() {
        console.log("in search bar function of tag validation");
        //document.getElementById("drop-opt").classList.toggle("show");
        $("#drop-valid-opt").show();

        var type = document.getElementsByClassName("validated-cell-inp");
        // If positional is asked to show
        $("#val").click(function() {
            for (var i=0; i < type.length; i++) {
                var value = type[i].firstChild;
                if (value) {
                    var result = value.textContent || value.innerText;
                    if (result.indexOf("true") > -1) {
                        //console.log("row"+i);
                        document.getElementById("row"+i).style.display = "";
                    } else {
                        document.getElementById("row"+i).style.display = "none";
                    }
                }
            }
        });
        // if general is asked to show
        $("#non-val").click(function () {
            for (var i=0; i < type.length; i++) {
                var value = type[i].firstChild;
                if (value) {
                    var result = value.textContent || value.innerText;
                    if (result.indexOf("false") > -1) {
                        //console.log("row"+i);
                        document.getElementById("row"+i).style.display = "";
                    } else {
                        document.getElementById("row"+i).style.display = "none";
                    }
                }
            }
        });

        // if all is asked to show
        $("#all-valid").click(function () {
            for (var i=0; i < type.length; i++) {
                document.getElementById("row"+i).style.display = "";
            }
        });

        // hide dropdown options
        window.onclick = function(event) {
            if (!event.target.matches('#drop-valid-btn')) {
                $("#drop-valid-opt").hide();
            }
        }
    });
}

/**
 * Function for retrieving the error message when an ajax request fails
 * @param xhr
 * @param status
 */
function getErrorMessage(xhr, status, message) {
    //console.log(message);
    console.log("error", xhr.status);
    var msg = '';
    if (xhr.status === 0) {
        msg = 'Not connect.\n Verify Network for request '+message;
    } else if (xhr.status == 404) {
        msg = 'Requested page not found. [404] for request '+message;
    } else if (xhr.status == 500) {
        msg = 'Internal Server Error [500] for request '+message;
    } else if (status === 'parsererror') {
        msg = 'Requested data parse failed for request '+message;
    } else if (status === 'timeout') {
        msg = 'Time out error for request '+message;
    } else if (status === 'abort') {
        msg = 'Ajax request aborted for request '+message;
    } else {
        msg = 'Uncaught Error for request.' + message +'\n' + xhr.responseText;
    }
    $(".div_right").append('<div>' + msg + '</div>');
}