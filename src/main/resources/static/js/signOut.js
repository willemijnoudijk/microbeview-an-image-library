// LOGOUT FUNCTIONALITY
$(document).ready(function () {
    $(".signout_button").on( "click", function() {
        $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="_csrf"]').attr('content') }
        });

        $.ajax({
            type: "POST",
            url: "/logout",
            success: function () {
                location.href = "/"
            }
        });
    });
});