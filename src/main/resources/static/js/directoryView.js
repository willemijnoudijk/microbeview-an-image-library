/**
 * JS file that contains the main functionality for the image library
 *
* @author
* initialized by Kylie
* edited by Willemijn
 */

// Instantiate needed variables
var current_folder;
var current_obj;
var current_direction;
var current_history;
var current_main_folder;
var current_main_data;

var back_array = [];
var index;
var back_index;
var crumb_folders = {};
var folder_array = [];

var folder_name;
var path;
var path_items;

var dir_options = document.getElementsByClassName("dir_options");

$(document).ready(function() {
    back_index = 0;
    index = 0;
    //set div height

    var currentFolderName = getCookie("folder");
    if (currentFolderName != "") {
        viewdir(currentFolderName.split("folder=")[1]);
    } else {
        getData();
    }


    $('#dir_options').prop('disabled', true);

    /**
     Clicking outside of an element itself
     */
    $(document).mouseup(function (e) {
        // Hide creating new folder form
        var new_folder_form = $('#directory');
        if (!new_folder_form.is(e.target) && new_folder_form.has(e.target).length === 0) {
            new_folder_form.hide();
        }

        // Hide folder options
        var rename_dir_form = $('#rename_dir');
        var fol_options = $('#dir_options');
        if (!fol_options.is(e.target) && fol_options.has(e.target).length === 0 &&
            !rename_dir_form.is(e.target) && rename_dir_form.has(e.target).length === 0) {
            $(dir_options).css("transition", "none");
            $(dir_options).css("border", "2px solid grey");
            $(dir_options).css("background-color", "grey");
            $(dir_options).prop('disabled', true);
        }
        // Hide rename directory form
        if (!rename_dir_form.is(e.target) && rename_dir_form.has(e.target).length === 0) {
            rename_dir_form.hide();
        }

        // Hide image options
        var rename_img_form = $('#rename_img');
        var img_options = $('#img_options');
        if (!img_options.is(e.target) && img_options.has(e.target).length === 0 &&
            !rename_img_form.is(e.target) && rename_img_form.has(e.target).length === 0) {
            img_options.hide();
            $(dir_options).css("transition", "transform 0.2s ease-out");
            $('#dir_options').prop('disabled', true);
            $('#dir_options').show();
            $('#fail_rename').innerHTML = "";
        }
        // Hide rename image form
        if (!rename_img_form.is(e.target) && rename_img_form.has(e.target).length === 0) {
            rename_img_form.hide();
        }
    });

    // Create the functionality for adding a new directory
    $('#dir_options_create').click( function (e) {
        e.preventDefault();
        $('#directory').show();
        $('#dir_upload_form')[0].reset();
    });
    $('#dir_upload_form').submit( function (e) {
        e.preventDefault();
        var new_folder = $('[name="newdir"]').val();
        addDirectory(new_folder);
        calculateDivHeights();
        $('#directory').hide();

    });

    // Create the functionality for renaming a directory
    $('#dir_options_rename').click(function (e) {
        e.preventDefault();
        $('#new_dir_name').attr('placeholder', folder_name.split(/[/\\]+/).pop());
        $('#rename_dir').show();
    });
    $('#rename_form').submit(function (e) {
        e.preventDefault();
        var new_folder_name = $('[name="new_dir_name"]').val();
        if (isValidName(new_folder_name)) {
            renameDirectory(folder_name, new_folder_name);
        } else {
            var fail_rename = document.getElementById("fail_rename");
            fail_rename.innerHTML = "New folder name '" + new_folder_name + "' contains invalid character(s)";
            $('#rename_form')[0].reset();
            $(fail_rename).delay(500).fadeIn();
            $(fail_rename).delay(5000).fadeOut();
        }
        $('#rename_dir').hide();
    });

    // Create the functionality for deleting a directory
    $('#dir_options_delete').click(function (e) {
       e.preventDefault();
       if (confirm("Are you sure you want to delete folder '" + folder_name + "'?")) {
           deleteDirectory(folder_name);
       }
    });

    /**
     * Function that will be called when back or forward button is clicked
    */
     $(window).on('popstate', function (e) {
        $('#dir_options').hide();
        $('#directory').hide();

        var state = e.originalEvent.state;
        if (state !== null) {

            var content = event.state.folder;
            var dir = event.state.dir;
            var main_folder = event.state.main_folder;
            var main_data = event.state.main_data;

            $('#directory_files').html("");
            $('#directory_folders').html("");
            index = 0;
            folder_array = [];

            viewdir(content, dir, false, main_folder, main_data);
        }
    });
});

/**
 * Ajax request for getting the image library
 */
function getData() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "/imagelibrary",
        success: function (data) {

            var main_data = data;
            var main_folder = data.name;

            crumb_folders[main_folder] = main_data;

            viewdir(data, true, true, main_folder, main_data);
            calculateDivHeights();
        },
        error: function (xhr, status) {
            console.log("inside error");
            getErrorMessage(xhr, status, "get original imagelibrary with images");
        }
    });
}

/**
 * Function that retrieves the updated library data when an edit is done or the page is refreshed
 * @param current_folder
 * @param current_direction
 * @param current_history
 * @param current_main_folder
 * @param current_main_data
 */
function getUpdatedData(current_folder, current_direction, current_history,
                        current_main_folder, current_main_data) {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "/imagelibrary",
        success: function (data) {
            var current_data = searchFolder(data, 'name', (k, v) => v === current_folder);
            $('#directory_files').empty();
            $('#directory_folders').empty();

            viewdir(current_data, current_direction, current_history, current_main_folder, current_main_data);
        },
        error: function (xhr, status) {
            console.log("inside error");
            getErrorMessage(xhr, status, "get imagelibrary with edit or after refresh");
        }
    });
}

/**
 * Function for retrieving the error message when an ajax request fails
 * @param xhr
 * @param status
 */
function getErrorMessage(xhr, status, message) {
    //console.log(message);
    console.log("error", xhr.status);
    var msg = '';
    if (xhr.status === 0) {
        msg = 'Not connect.\n Verify Network for request '+message;
    } else if (xhr.status == 404) {
        msg = 'Requested page not found. [404] for request '+message;
    } else if (xhr.status == 500) {
        msg = 'Internal Server Error [500] for request '+message;
    } else if (status === 'parsererror') {
        msg = 'Requested data parse failed for request '+message;
    } else if (status === 'timeout') {
        msg = 'Time out error for request '+message;
    } else if (status === 'abort') {
        msg = 'Ajax request aborted for request '+message;
    } else {
        msg = 'Uncaught Error for request.' + message +'\n' + xhr.responseText;
    }
    $(".div_right").append('<div>' + msg + '</div>');
}

function searchFolder(json, name, folder_name) {
    if (json.hasOwnProperty(name) && folder_name(name, json[name]) === true) {
        return json;
    }
    for (let i = 0; i < Object.keys(json).length; i++) {
        if (typeof json[Object.keys(json)[i]] === "object") {
            let o = searchFolder(json[Object.keys(json)[i]], name, folder_name);
            if (o != null) {
                return o;
            }
        }
    }
    return null;
}

/** Function that pushes the current state to the history
 * @param directoryObj
 * @param direction
 * @param main_folder
 * @param main_data
 */
function pushState(directoryObj, direction, main_folder, main_data) {
    history.pushState({folder: directoryObj,
        dir: direction,
        main_folder: main_folder,
        main_data: main_data}, "previous_folder");
}

/** Function that creates an array of all folders to current folder
 * @param folderPath
 * @param main_data
 * @param direction
 * @param pushHistory
 * @param main_folder
 */
function createBreadcrumbs(folderPath, main_data, direction, pushHistory, main_folder) {
    if (folderPath.name.includes("/")) {
        var path = folderPath.name.slice(folderPath.name.indexOf('MicrobeView'));
        path_items = path.split("/").slice(1);
        console.log("Breadcrumbs = " + path_items);
    } else if (folderPath.name.includes("\\")) {
        var path = folderPath.name.slice(folderPath.name.indexOf('MicrobeView'));
        path_items = path.split("\\").slice(1);
        console.log("Breadcrumbs = " + path_items);
    }
    showBreadcrumbs(path_items, folderPath, main_data, direction, pushHistory, main_folder);
}

/** Functions that shows the breadcrumbs on the page
 * @param breadcrumbs
 * @param current_data
 * @param main_data
 * @param direction
 * @param pushHistory
 * @param main_folder
 */
function showBreadcrumbs(breadcrumbs, current_data, main_data, direction, pushHistory, main_folder) {
    $('#breadcrumb').html("");
    var crumb_count = 0;

    console.log(current_data.name);
    console.log(crumb_folders);

    for (var i=0; i<breadcrumbs.length;i++){
        if (breadcrumbs.length === 1) {
            $('#breadcrumb'). append(breadcrumbs[i]);
        }
        else {
            crumb_count++;
            if (i === (breadcrumbs.length - 1)){
                $('#breadcrumb').append(' ' + breadcrumbs[i]);
            } else {
                $('#breadcrumb').append('<div id="crumbdiv'+crumb_count+'" class="crumbdiv">' +
                    '&#160;<u>' + breadcrumbs[i] + '</u> > </div>');
                console.log('<div id="crumbdiv'+crumb_count+'">' +
                    '<u>' + breadcrumbs[i] + '</u> > </div>');

                $('#breadcrumb').on('click', '#crumbdiv'+crumb_count, function () {
                    console.log(crumb_folders[current_data.name]);
                    // viewdir(current_data, direction, pushHistory, main_folder, main_data);
                });
            }
        }
    }
}

/** Function that creates the overall view of the image library
 * @param jsonObj
 * @param direction
 * @param pushHistory
 * @param main_folder
 * @param main_data
 */
function viewdir(jsonObj, direction, pushHistory, main_folder, main_data) {

    current_direction = direction;
    current_history = pushHistory;
    current_main_folder = main_folder;
    current_main_data = main_data;

    // Check if data is already a JSON object
    if (typeof jsonObj === 'string') {
        jsonObj = JSON.parse(jsonObj);
    }
    current_obj = jsonObj;
    current_folder = jsonObj.name;

    console.log("Current folder = " + jsonObj.name,
        ", Direction = " + direction);

    setCookie(JSON.stringify(current_obj));

    // Push to history if new page is loaded
    if (pushHistory) {
        pushState(jsonObj, direction, main_folder, main_data);
    }

    $('#back_folder').html("");

    if (direction) {
        back_array[back_index] = jsonObj;
    }

    if (back_index > 0) {
        $('#back_folder').html('<img class="backfolder_button" id="backfolder_button" data-backindex="' + back_index + '" ' +
            'src="images/back.png" class="dir_img"/>');
    }

    // hide back folder if current folder is the main directory
    if (jsonObj.name === main_folder) {
        $('#back_folder').html("");
    }

    for (var file in jsonObj.files) {
        path = jsonObj.files[file].name.replace(/\\/g, "/");

        // make sure only thumbnails are viewed on the left side
        if (path.indexOf("_thumb") >= 0)
        {
            var filename = path.substring(path.lastIndexOf("/") + 1, path.length);
            // Get original image name of thumbnail image
            var image_name = filename.substr(0, filename.indexOf("_thumb"));
            var thumbnailLength = filename.length;
            var extensionStart = filename.indexOf(".");
            var image_extension = filename.substr(extensionStart, thumbnailLength);
            // The imagename without the _thumb
            var image_src = image_name.concat(image_extension);
            if(image_src.length > 18) {
                var part1 = image_src.substring(0,8);
                var part2 = image_src.substring(image_src.length - 8, image_src.length);
                var new_img_src = part1 + ".." + part2;

            } else {
                new_img_src = image_src;
            }

            // this is for linux: path starts with slash
            if (path.charAt(0) === "/") {
                $('#directory_files').append(
                    '<div class="column"><img src="' +
                    path + ' " onclick="expandClickedImage(this);"/><br><p class="filename">' +
                    new_img_src + '</p></div>');
            } // this is for windows: path starts without a slash
            else {
                $('#directory_files').append(
                    '<div class="column"><img src="' +
                    "/" + path + ' " onclick="expandClickedImage(this);"/><br><p class="filename">' +
                    new_img_src + '</p></div>');
            }
        }
    }

    /**
     * Show all the folders on the page
     */
    for (var folder in jsonObj.folders) {

        $('#directory_folders').append(

            '<div class="folderdiv"><img class="folder_button" id="folder_button" data-folderindex="'+index+'" ' +
            'src="images/directory.png" class="dir_img"/><br>' +
            '<label class="folder_button_label" for="folder_button">' + jsonObj.folders[folder].name.split(/[/\\]+/).pop() + '</label></div>');
        $(".folder_button").css("width", "100px");
        $(".folder_button").css("heigth", "100px");
        $(".folderdiv").css("width", "100px");

        folder_name = jsonObj.folders[folder].name;
        if (!crumb_folders.hasOwnProperty(folder_name)) {
            crumb_folders[folder_name] = jsonObj.folders[folder];
        }
        folder_array.push(JSON.stringify(jsonObj.folders[folder]));
        index++;
    }

    createBreadcrumbs(jsonObj, main_data, direction, pushHistory, main_folder);

    /**
     * Click on a folder to make renaming and deleting of a folder possible
     */
    $('*[data-folderindex]').click(function (e) {
        e.preventDefault();
        folder_name = jsonObj.folders[folder].name;
        $('#dir_options').hide();
        $(dir_options).css("background", "#39ae3f");
        $(dir_options).css("border", "2px solid #39ae3f");
        $(dir_options).prop('disabled', false);
        $('#dir_options').show();

        var folder_index = $(this).attr('data-folderindex');
        folder_name = JSON.parse(folder_array[folder_index]).name;
    });

    /**
     * Dubble click a folder to go to the next folder
     */
    $('*[data-folderindex]').dblclick(function () {
        $('#directory').hide();

        $(dir_options).css("transition", "none");
        $(dir_options).css("border", "2px solid grey");
        $(dir_options).css("background-color", "grey");
        $(dir_options).prop('disabled', true);

        var folder_index = $(this).attr('data-folderindex');
        var next_dir = folder_array[folder_index];

        $('#directory_files').html("");
        $('#directory_folders').html("");
        index = 0;
        back_index++;
        folder_array = [];

        viewdir(next_dir, true, true, main_folder, main_data);

        calculateDivHeights();

    });

    /**
     * Dubble click the back button to go to the previous folder
     */
    $('#backfolder_button').dblclick(function () {
        $('#directory').hide();

        var backfolder_index = $(this).attr('data-backindex');
        var next_dir = back_array[backfolder_index - 1];

        $('#directory_files').html("");
        $('#directory_folders').html("");
        index = 0;
        back_index = back_index - 1;
        folder_array = [];

        getUpdatedData(next_dir.name);

        calculateDivHeights();
    });

}

/**
 * @function dinamically calculate the element sizes for the browser part
 *
 * Calculates the element sizes based on browser resolution
 */
function calculateDivHeights() {
    setTimeout(function() {
        var row_height = ($(window).height() * 0.75);
        var dir_folders_height = ($(".directory_folders").height() + $(".breadcrumb").height() + 20);
        if ($(".backfolder_button").height() > 0) {
            dir_folders_height += $(".backfolder_button").height();
        }
        console.log("dir folders height: " + $(".directory_folders").height());
        console.log("breadcrumb height: " + $(".breadcrumb").height());
        console.log("backfolder height: " + $(".backfolder_button").height());

        var height_for_filesdiv = row_height - dir_folders_height;
        console.log("new files height: " + height_for_filesdiv);
        $(".row").css("height", row_height);
        $(".directory_files").css("height", height_for_filesdiv);}, 250);
}

/**
 * Set cookie used when refreshing the page in the current folder
 * @param folder
 */
function setCookie(folder) {
    var d = new Date();
    d.setTime(d.getTime() + (10*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "folder=" + folder + ";" + expires + ";path=/";
}
function getCookie(folder) {
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(folder + '=') == 0) {
            var foldername =  c.substring(folder + '='.length, c.length);
            return foldername;
        }
    }
    return "";
}