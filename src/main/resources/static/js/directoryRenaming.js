/**
 * Method that creates functionality for renaming the selected directory
 *
 * @author Kylie
 */
function renameDirectory(selected_folder, new_folder_name, current_obj, current_direction, current_history,
    current_main_folder, current_main_data) {
    var xhr = new XMLHttpRequest();
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    var params = "selectedFolder="+selected_folder+"&newFolderName="+new_folder_name;

    xhr.open('POST', '/folderrenaming', true);
    xhr.setRequestHeader(header, token);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(params);

    console.log("Renamed folder: " + selected_folder + " to: " + new_folder_name);

    xhr.onreadystatechange = function() {
        if (xhr.status === 200) {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                getUpdatedData(current_folder, current_direction, current_history,
                    current_main_folder, current_main_data);
            }
        } else if (xhr.status === 500) {
            $(".div_left").append(
                '<div id="error-msg-diradd" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;Error; there is something wrong with renaming the folder ' +selected_folder+ '.</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        } else {
            $(".div_left").append(
                '<div id="error-msg-diradd" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>&nbsp;Error renaming folder ' + selected_folder + '</div>');
            $(".error-msg").delay(400).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        }
    }
}