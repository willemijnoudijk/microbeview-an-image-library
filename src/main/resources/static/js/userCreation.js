$(document).ready(function () {
    console.log("document ready");

    $(".create-user-button").onclick = function () {
        location.href = "/create-user";
    };

    $("#new-password").change(validateInput);
    $("#new-username").change(validateInput);
    $("#new-email").change(validateInput);

    var create_user_btn = document.getElementsByClassName("create-new-user-btn");

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function validateInput() {
        if ($('#new-username').val().length > 0 && $('#new-email').val().length > 0 && $('#new-password').val().length > 0
            && $("#new-password").val().length >= 8 && validateEmail($("#new-email").val())) {
            console.log(".tag_input val > 0");
            $(create_user_btn).prop("disabled", false);
            $(create_user_btn).css("background-color", "#39ae3f");
            $(create_user_btn).css("cursor", "pointer");
            $(".status-message").css("color", "#39ae3f");
            $(".status-message").html("Everything is correct. If you're sure, hit the create button!");
            $(".status-message").show();
        }
        else {
            $(create_user_btn).prop("disabled", true);
            $(create_user_btn).css("background-color", "grey");
            $(create_user_btn).css("cursor", "default");
            $(".status-message").css("color", "red");
            $(".status-message").html(
                "Please fill out all fields.<br>" +
                "Make sure the password is eight characters or longer<br>" +
                "Make sure the email-address is an actual address"
            );
            $(".status-message").show();


        }
    }

    $(".create-new-user-btn").click(function(e) {
        console.log("submit clicked");
        e.preventDefault();
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        var form = document.getElementById("create-user-form");
        var email = $("#new-email").val();
        var username = $("#new-username").val();
        var password = $("#new-password").val();

        var radios = document.getElementsByName('new-role');

        for (var i = 0, length = radios.length; i < length; i++)
        {
            if (radios[i].checked)
            {
                // do whatever you want with the checked radio
                var role = radios[i].value;

                // only one radio can be logically checked, don't check the rest
                break;
            }
        }

        var formData = new FormData();
        formData.append("new-email", email);
        formData.append("new-username", username);
        formData.append("new-password", password);
        formData.append("new-role", role);

        $.ajax({
            url: '/create',
            beforeSend: function(request) {
                request.setRequestHeader(header, token);
            },
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST'
        }).done(function( data ) {
            $(".create-user-form").append(
                '<div id="success-msg-create-user" class="success-msg">' +
                '<i class="fa fa-check"></i>&nbsp;' +
                'You successfully created user '+ email +'' +
                '</div>');
            $(".success-msg").delay(400).fadeIn();
            $(".success-msg").delay(5000).fadeOut();
        }).fail(function( data ) {
            var data2 = JSON.parse(data.responseText);
            if(data2.message.includes("Duplicate entry")) {
                $(".create-user-form").append(
                    '<div id="error-msg-create-user" class="error-msg"> ' +
                    '<i class="fa fa-times-circle"></i>&nbsp;' +
                    'This email-address is already in use' +
                    '</div>');
                $(".error-msg").delay(400).fadeIn();
                $(".error-msg").delay(5000).fadeOut();
            } else {
                $(".create-user-form").append(
                    '<div id="error-msg-create-user" class="error-msg"> ' +
                    '<i class="fa fa-times-circle"></i>&nbsp;' +
                    'Creation of user failed' +
                    '</div>');
                $(".error-msg").delay(400).fadeIn();
                $(".error-msg").delay(5000).fadeOut();
            }
        });
        $(create_user_btn).prop("disabled", true);
        $(create_user_btn).css("background-color", "grey");
        $(create_user_btn).css("cursor", "default");
        $(".error-msg").remove();
        $(".success-msg").remove();
        form.reset();
        $(".status-message").html('');
        $(".status-message").hide();
    });
});