/**
 * @author Olle
 */

$(document).ready( function() {
    var VarStore = {};
    var uploadbutton = document.getElementsByClassName("upload_button");
    $(uploadbutton).prop('disabled', true);
    var label = 'label[for="' + $(this).attr('id') + '"]';
    var succes_upload = document.getElementById("succes_upload");
    var fail_upload = document.getElementById("fail_upload");
    // Function is executed when the file selector is value is changed
    // This changes the text content of the button to how many files are selected
    $('input[type="file"]').change( function() {
        $(uploadbutton).prop("disabled", false);

        //construct a selector for the label that belongs to the changed input field
        var para = document.getElementById("upload_file_text");
        var files = $(this)[0].files;
        VarStore.len = files.length;
        if(files.length > 1){
            $(para).text(files.length + " files selected");
            $(uploadbutton).css("transition", "all 0.2s ease-out");
            $(uploadbutton).css("background", "#39ae3f");
            $(uploadbutton).css("border", "2px solid #39ae3f");
        } else {
            var fn = $(this).val().split("\\")[2];
            $(para).text(fn);
            $(uploadbutton).css("transition", "all 0.2s ease-out");
            $(uploadbutton).css("background", "#39ae3f");
            $(uploadbutton).css("border", "2px solid #39ae3f");

        }
        $(para).css("font-size", "14px");
        $(para).css("margin-top", "13px");

    }).hide();

    // This function is excecuted whenever the file upload button is clicked
    // and it uploads the files to the desired folder.
    $("#upload_button").click( function(ev) {
        ev.preventDefault();
        var para = document.getElementById("upload_file_text");
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        var form = document.getElementById("upload_form");
        var formData = new FormData(form);
        /**
         * @function display succes or error message
         *
         * If status is 200, then show succes message and reload directory view
         * If status is NOT 200, then show custom error message
         */
        //$(".gooey").fadeIn();
        $(".loader").fadeIn();
        $.ajax({
            url: '/upload',
            beforeSend: function(request) {
                request.setRequestHeader(header, token);
            },
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST'
        }).done(function( data ) {
            $(".loader").fadeOut();
            $(".div_left").append(
                '<div id="success-msg-upload" class="success-msg">' +
                '<i class="fa fa-check"></i>' +
                ' You successfully uploaded ' + VarStore.len + ' file(s)!' +
                '</div>');
            $(".success-msg").delay(800).fadeIn();
            $(".success-msg").delay(5000).fadeOut();
            getUpdatedData(current_folder, current_direction, current_history, current_main_folder, current_main_data);
        }).fail(function( data ) {
            $(".loader").fadeOut();
            $(".div_left").append(
                '<div id="error-msg-upload" class="error-msg"> ' +
                '<i class="fa fa-times-circle"></i>' +
                ' An error occurred while trying to upload, please try again' +
                '</div>');
            $(".error-msg").delay(800).fadeIn();
            $(".error-msg").delay(5000).fadeOut();
        });
        $('.error-msg').remove();
        $('.success-msg').remove();

        //succes_upload.css("visibility", "hidden");


        console.log(form.accessKey);
        $(para).text('SELECT IMAGE(S)');
        $(uploadbutton).css("background", "grey");
        $(uploadbutton).css("border", "2px solid grey");
        $(uploadbutton).prop('disabled', true);
        $(label).css("font-size", "14px");
        $(para).css("margin-top", "14px");

    })
});

