DROP TABLE IF EXISTS ROIdata;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS metadata;
DROP TABLE IF EXISTS images;
DROP TABLE IF EXISTS keyvalue;

CREATE TABLE `users` (
  `id`           INT(11)          NOT NULL  AUTO_INCREMENT,
  `email`        VARCHAR(40)      NOT NULL,
  `username`     VARCHAR(40)      NOT NULL,
  `password`     VARCHAR(200)     NOT NULL,
  `role`         VARCHAR(5)       NOT NULL,
  `enabled`      INT(2)           DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
);

CREATE TABLE `metadata` (
  `id`                  INT(11)         NOT NULL  AUTO_INCREMENT,
  `xcoord`              FLOAT(25,5)     NULL,
  `ycoord`              FLOAT(25,5)     NULL,
  `tag`                 VARCHAR(100)    NOT NULL,
  `creator`             VARCHAR(60)     NOT NULL,
  `source`              VARCHAR(400)    NOT NULL,
  `date_of_creation`    VARCHAR(25)     NOT NULL,
  `validated`           boolean         NOT NULL DEFAULT FALSE,

   PRIMARY KEY (`id`)
);

CREATE TABLE `ROIdata` (
  `id`                  INT(11)         NOT NULL  AUTO_INCREMENT,
  `xcoord`              FLOAT(25,5)     NULL,
  `width`               FLOAT(25,5)     NULL,
  `ycoord`              FLOAT(25,5)     NULL,
  `height`              FLOAT(25,5)     NULL,
  `image_source`        VARCHAR(400)    NOT NULL,

   PRIMARY KEY (`id`)
);

CREATE TABLE `images` (
  `id`                  INT(11)         NOT NULL AUTO_INCREMENT,
  `file_name`           VARCHAR(400)    NOT NULL,
  `thumb_file_name`     VARCHAR(400)    NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `keyvalue` (
  `id`                  INT(11)         NOT NULL AUTO_INCREMENT,
  `tag_key`             VARCHAR(400)    NOT NULL,
  `tag_value`           VARCHAR(400)    NOT NULL,
  `creator`             VARCHAR(60)     NOT NULL,
  `image_source`        VARCHAR(400)    NOT NULL,
  `date`                VARCHAR(25)     NOT NULL,
  `validated`           VARCHAR(20)     NOT NULL DEFAULT 0,

  PRIMARY KEY (`id`)
)

  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;