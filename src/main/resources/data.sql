INSERT INTO users (email, username, password, role, enabled) VALUES
('willemijn@admin.nl', 'Willemijn Oudijk','$2a$10$qlFA/CIZMZSpFQ5Cfm0Wq.hOaJ9qmsgpBGSqurN747pA9PLfG170a', 'ADMIN', 1),
('kylie@user.nl', 'Kylie Keijzer','$2a$10$3x9CCfd6zHbi73O7cZs9l.9H/n5rBXtHey/m2iR7ksLUoWfJA.9mG', 'USER', 1),
('olle@user.nl', 'Olle de Jong','$2a$10$t3CpEkgBY3aD1o48sPYtk.gOdyHKgvknIFdVTSV5xf7vcy3BNuMk6', 'USER', 1);

INSERT INTO metadata (xcoord, ycoord, tag, creator, source, date_of_creation, validated) VALUES
('0.3336', '0.3336', 'test1', 'willemijn@admin.nl', '0083.jpg', '28/05/2019 - 12:45', FALSE),
('0.5336', '0.3636', 'test2', 'willemijn@admin.nl', '0083.jpg', '28/05/2019 - 12:45', FALSE),
('0.824', '0.2453', 'test3', 'willemijn@admin.nl', '0083.jpg', '28/05/2019 - 12:45', FALSE),
('0.57', '0.765', 'test4', 'willemijn@admin.nl', '0083.jpg', '28/05/2019 - 12:45', FALSE),
('0.88', '0.89', 'test5', 'willemijn@admin.nl', '0083.jpg', '28/05/2019 - 12:45', FALSE),
('0' , '0', 'test6', 'olle@admin.nl', '0083.jpg', '28/05/2019 - 12:45', FALSE),

('124.55', '124.523', 'ecoli2', 'olle@admin.nl', '0084.jpg', '28/05/2019 - 12:45', FALSE);


INSERT INTO images (file_name, thumb_file_name) VALUES
('0001.jpg', '0001_thumb.jpeg'),
('0002.jpg', '0002_thumb.jpeg');

INSERT INTO keyvalue (tag_key, tag_value, creator, date, image_source, validated) VALUES
('pH', '7', 'willemijn@admin.nl', '11/06/2019 - 15:25', '0083.jpg', FALSE),
('Oxygen', '20', 'willemijn@admin.nl', '11/06/2019 - 15:25', '0083.jpg', FALSE);

-- ('pH', '7'),
-- ('Oxygen', '20');




