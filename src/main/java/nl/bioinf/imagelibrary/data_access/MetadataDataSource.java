package nl.bioinf.imagelibrary.data_access;

import nl.bioinf.imagelibrary.model.Metadata;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * @author Olle
 * Edited by Willemijn
 */
public interface MetadataDataSource {
    ResponseEntity<Metadata> getMetadataById(Integer id);

    List<String> getSourceByTag(String tag);

    void storeXyMetadata(Double xcoord, Double ycoord, String tag, String creator, String source, String date);

    void storeGeneralMetadata(String tag, String creator, String source, String date);

    ResponseEntity getMetadataBySource(String imageSource);

    void validateTag(String date_of_creation, String source, Boolean validated, String tag_name);

    void removeMetadataInstance(String date_of_creation, String source, String tag_name);
}

