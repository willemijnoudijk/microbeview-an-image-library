package nl.bioinf.imagelibrary.data_access;

import nl.bioinf.imagelibrary.model.Image;

/**
 * @author Willemijn
 */

public interface ImageDataSource {
    Image getImageById(String id);
    void storeImage(String fileName, String thumbFileName);
}
