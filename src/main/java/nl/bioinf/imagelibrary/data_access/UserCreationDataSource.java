package nl.bioinf.imagelibrary.data_access;

public interface UserCreationDataSource {
    void createUser(String email, String username, String password, String role);
}
