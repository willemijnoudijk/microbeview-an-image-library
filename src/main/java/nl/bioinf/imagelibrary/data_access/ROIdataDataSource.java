package nl.bioinf.imagelibrary.data_access;

import nl.bioinf.imagelibrary.model.RoiData;

import java.util.ArrayList;

/**
 * Interface for retrieving and storing ROI data
 *
 * @author Kylie
 */
public interface ROIdataDataSource {
    RoiData getRoiDataById(String id);

    void storeROIData(Double xcoord, Double width,
                      Double ycoord, Double height, String imageSource);

    ArrayList<RoiData> getROIsByImageSource(String imageSource);
}
