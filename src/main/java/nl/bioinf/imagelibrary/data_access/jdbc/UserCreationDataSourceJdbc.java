package nl.bioinf.imagelibrary.data_access.jdbc;

import nl.bioinf.imagelibrary.config.PasswordEncoderTest;
import nl.bioinf.imagelibrary.data_access.UserCreationDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class UserCreationDataSourceJdbc implements UserCreationDataSource {

    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.data_access.jdbc.UserCreationDataSourceJdbc");

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    public UserCreationDataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    @Override
    public void createUser(String email, String username, String password, String role) {
        LOGGER.info("Attempting to create new user");
        LOGGER.info(email+ " " + password);
        try {
            PasswordEncoderTest passwordEncoderTest = new PasswordEncoderTest();
            String hashedPassword = passwordEncoderTest.encodePassword(password);
            jdbcTemplate.update("INSERT INTO users (email, username, password, role) VALUES (?, ?, ?, ?)",
                    email, username, hashedPassword, role);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Exception was catched when trying create a new user:" + e.getMessage());
            throw e;
        }
    }
}
