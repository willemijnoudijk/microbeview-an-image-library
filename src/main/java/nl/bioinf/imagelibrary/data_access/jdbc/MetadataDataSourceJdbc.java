package nl.bioinf.imagelibrary.data_access.jdbc;

import nl.bioinf.imagelibrary.data_access.MetadataDataSource;
import nl.bioinf.imagelibrary.model.Metadata;
import nl.bioinf.imagelibrary.service.StorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.swing.plaf.metal.MetalTabbedPaneUI;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that serves the functionality for the Database interaction of the metadata
 * @author Olle
 * Edited by Willemijn
 */
@Component
public class MetadataDataSourceJdbc implements MetadataDataSource {

    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.data_access.jdbc.MetadataDataSourceJdbc");

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;


    @Autowired
    public MetadataDataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = namedJdbcTemplate;
    }


    /**
     * Gets metadata instance using param id and maps this
     * to a Metadata DTO.
     * @param id
     * @return ResponseEntry(Metadata DTO, HttpStatus.OK)
     */
    @Override
    public ResponseEntity<Metadata> getMetadataById(Integer id) {
        LOGGER.log(Level.INFO, "metadata id:" + id);
        Metadata metadataObject = namedJdbcTemplate.queryForObject("SELECT * FROM metadata WHERE id =:id",
                new MapSqlParameterSource("id", id),
                BeanPropertyRowMapper.newInstance(Metadata.class));
        //metadataObject.add();

        return new ResponseEntity(metadataObject, HttpStatus.OK);
    }


    @Override
    public List<String> getSourceByTag(String tag) {
        List<String> result = namedJdbcTemplate.queryForList("SELECT source FROM metadata WHERE tag=:tag",
                new MapSqlParameterSource("tag", tag), (String.class));
        return result;
    }

    /**
     * Stores a positional metadata tag instance by inserting it into the metadata table.
     * @param xcoord x-coordinate of the positional tag
     * @param ycoord y-coordinate of the positional tag
     * @param tag the tag name
     * @param creator creator of the the tag
     * @param source filename / sourcename to which the tag belongs
     * @param date date of creation
     */
    @Override
    public void storeXyMetadata(Double xcoord, Double ycoord, String tag, String creator, String source, String date) {
        LOGGER.log(Level.INFO, "Storing a point with metadata, tag=" + tag + "on image:" + source);
        try {
            jdbcTemplate.update("INSERT INTO metadata (xcoord, ycoord, tag, creator, source, date_of_creation) VALUES (?, ?, ?, ?, ?, ?)",
                    xcoord, ycoord, tag, creator, source, date);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Exception was catched when trying to store xy-metadata:" + e.getMessage());
            throw e;
        }
    }

    /**
     * Stores a general metadata tag instance by inserting it into the metadata table.
     * xcoord and ycoord of an general tag is always 0.0 since it has no coordinates.
     * @param tag the tag name
     * @param creator creator of the the tag
     * @param source filename / sourcename to which the tag belongs
     * @param date date of creation
     */
    @Override
    public void storeGeneralMetadata(String tag, String creator, String source, String date) {
        LOGGER.log(Level.INFO, "storing general metadata:" + tag + "on image:" + source);
        try {
            jdbcTemplate.update("INSERT INTO metadata (xcoord, ycoord, tag, creator, source, date_of_creation) VALUES (0, 0, ?, ?, ?, ?)",
                    tag, creator, source, date);
        } catch (StorageException e) {
            LOGGER.log(Level.WARNING, "Exception was catched when trying to store general metadata, exception:" + e.getMessage());
            throw e;
        }
    }

    /**
     * Retrieves all metadata for that particular source.
     * @param source the sourcename / filename for which the data is retrieved
     * @return ResponseEntity(Metadata DTO, HttpStatus.OK)
     */
    @Override
    public ResponseEntity<Metadata> getMetadataBySource(String source) {
        try {
            String sql = "SELECT * FROM metadata WHERE source =:source";
            List<Metadata> metadataList = namedJdbcTemplate.query(sql, new MapSqlParameterSource("source", source), new BeanPropertyRowMapper<>(Metadata.class));
            LOGGER.info("Retrieved all metadata for image with name " + source);
            if(metadataList.isEmpty()) {
                return new ResponseEntity("NO METADATA AVAILABLE", HttpStatus.OK);
            }
            return new ResponseEntity(metadataList, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Exception was catched when trying retrieve a list of the existing metadata, exception:" + e.getMessage());
            throw e;
        }

    }

    /**
     * Updates the field 'validated' to true so that the tag
     * has been validated.
     * @param date_of_creation date of creation of the metadata tag
     * @param source sourcename / filename of the image
     * @param validated value that it needs to be changed to (true)
     * @param tag_name the tag name of the metadata instance that needs to be validated
     */
    @Override
    public void validateTag(String date_of_creation, String source, Boolean validated, String tag_name) {
        try {
            jdbcTemplate.update("UPDATE metadata SET validated = ? WHERE source = ? AND date_of_creation = ? AND tag = ?", validated, source, date_of_creation, tag_name);
        } catch (StorageException e) {
            LOGGER.log(Level.WARNING, "Exception was catched when trying to validate tag, exception:" + e.getMessage());
            throw e;
        }

    }


    /**
     * Removal of a metadata tag instance.
     * @param date_of_creation date of creation of the metadata tag
     * @param source sourcename / filename of the image
     * @param tag_name the name of the metadata tag
     */
    @Override
    public void removeMetadataInstance(String date_of_creation, String source, String tag_name) {
        try {
            LOGGER.info("date = " + date_of_creation + " and source = " + source + " and tag = " + tag_name);
            jdbcTemplate.update("DELETE FROM metadata WHERE date_of_creation = ? and source = ? and tag = ?", date_of_creation, source, tag_name);
            LOGGER.info("Deleted instance from the metadata table!");
        } catch (StorageException e) {
            LOGGER.log(Level.WARNING, "Exception was catched when trying to remove this metadata instance, exception:" + e.getMessage());
            throw e;
        }
    }
}