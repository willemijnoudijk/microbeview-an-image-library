package nl.bioinf.imagelibrary.data_access.jdbc;

import nl.bioinf.imagelibrary.data_access.KeyValueDataSource;
import nl.bioinf.imagelibrary.model.KeyValue;
import nl.bioinf.imagelibrary.service.StorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that serves the functionality for the Database interaction of the key-value pairs
 * @author Willemijn
 */

@Component
public class KeyValueDataSourceJdbc implements KeyValueDataSource {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.data_access.jdbc.KeyValueDataSourceJdbc");

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    public KeyValueDataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    @Override
    public KeyValue getKeyValueById(String id) {
        LOGGER.log(Level.INFO, "Getting key-value pairs by id " + id);
        return namedJdbcTemplate.queryForObject("SELECT * FROM keyvalue WHERE id =:id",
                new MapSqlParameterSource("id", id),
                BeanPropertyRowMapper.newInstance(KeyValue.class));
    }

    @Override
    public void storeKeyValue(String key, String value, String creator, String date, String imageSource) {
        LOGGER.log(Level.INFO, "Storing key-value pair of key "+key+" with value "+value+" of image "+imageSource);
        jdbcTemplate.update("INSERT INTO keyvalue (tag_key, tag_value, creator, date, image_source) VALUES (?, ?, ?, ?, ?)",
                key, value, creator, date, imageSource);
    }

    @Override
    public List<KeyValue> getKeyValueBySource(String image_source) {
        LOGGER.log(Level.INFO, "Retrieving key-value pairs of image "+image_source);
        List<KeyValue> result = namedJdbcTemplate.query("SELECT * FROM keyvalue WHERE image_source =:image_source",
                new MapSqlParameterSource("image_source", image_source),
                new BeanPropertyRowMapper<>(KeyValue.class));
        return result;
    }

    @Override
    public void validateKeyValue(String value, String date, String source, Boolean validated) {
        LOGGER.info("inside keyvaluedatasourcejdbc");
        try {
            jdbcTemplate.update("UPDATE keyvalue SET validated = ? WHERE date = ? AND (image_source = ?) AND (tag_value = ?)", validated, date, source, value);
        } catch (StorageException e) {
            LOGGER.log(Level.WARNING, "Exception was catched when trying to validate key-value pair, exception:" + e.getMessage());
            throw e;
        }

    }
}
