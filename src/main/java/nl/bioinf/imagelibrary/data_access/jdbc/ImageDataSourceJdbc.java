package nl.bioinf.imagelibrary.data_access.jdbc;

import nl.bioinf.imagelibrary.data_access.ImageDataSource;
import nl.bioinf.imagelibrary.model.Image;
import nl.bioinf.imagelibrary.service.StorageException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that serves the functionality for the Database interaction of the images
 * @author Willemijn
 */

@Component
public class ImageDataSourceJdbc implements ImageDataSource {

    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.data_access.jdbc.ImageDataSourceJdbc");

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    public ImageDataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = namedJdbcTemplate;
    }


    @Override
    public Image getImageById(String id) {
        LOGGER.log(Level.INFO, "Getting image by id: " + id);
        return namedJdbcTemplate.queryForObject("SELECT * FROM images WHERE id =:id",
                new MapSqlParameterSource("id", id),
                BeanPropertyRowMapper.newInstance(Image.class));
    }

    @Override
    public void storeImage(String fileName, String thumbFileName) {
        LOGGER.log(Level.INFO, "storing image:" + fileName);
        try {
            jdbcTemplate.update("INSERT INTO images (file_name, thumb_file_name) VALUES (?, ?)",
                    fileName, thumbFileName);
            } catch (StorageException e) {
            throw new StorageException("Failed to store image:" + fileName);
        }
    }
}
