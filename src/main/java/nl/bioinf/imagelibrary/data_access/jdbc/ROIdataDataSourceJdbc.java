package nl.bioinf.imagelibrary.data_access.jdbc;

import nl.bioinf.imagelibrary.data_access.ROIdataDataSource;
import nl.bioinf.imagelibrary.model.RoiData;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller that serves functionality for storing in and retrieving data from ROIdata table in the database
 *
 * @author Kylie
 */
@Component
public class ROIdataDataSourceJdbc implements ROIdataDataSource {

    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.data_access.jdbc.ROIdataDataSourceJdbc");
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    public ROIdataDataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    @Override
    public RoiData getRoiDataById(String id) {
        LOGGER.log(Level.INFO, "Getting roi data with id: " + id);
        return namedJdbcTemplate.queryForObject("SELECT * FROM ROIdata WHERE id =:id",
                new MapSqlParameterSource("id", id),
                BeanPropertyRowMapper.newInstance(RoiData.class));
    }

    @Override
    public void storeROIData(Double xcoord, Double width, Double ycoord, Double height, String imageSource) {
        try {
            LOGGER.log(Level.INFO, "Storing coordinates from created ROI for image: " + imageSource);
            jdbcTemplate.update("INSERT INTO ROIdata (xcoord, width, ycoord, height, image_source) VALUES " +
                    "           (?, ?, ?, ?, ?)", xcoord, width, ycoord, height, imageSource);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ArrayList<RoiData> getROIsByImageSource(String imageSource) {
        LOGGER.log(Level.INFO, "Retrieving ROIs from image: " + imageSource);
        String sql = "SELECT * FROM ROIdata WHERE image_source =:image_source";
        ArrayList<RoiData> ROIs = (ArrayList<RoiData>) namedJdbcTemplate.query(sql, new MapSqlParameterSource("image_source",
                                                          imageSource), new BeanPropertyRowMapper<>(RoiData.class));
        return ROIs;
    }
}
