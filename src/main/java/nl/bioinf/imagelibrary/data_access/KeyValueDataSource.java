package nl.bioinf.imagelibrary.data_access;

import nl.bioinf.imagelibrary.model.KeyValue;

import java.util.List;

/**
 * @author Willemijn
 */

public interface KeyValueDataSource {
    KeyValue getKeyValueById(String id);

    void storeKeyValue(String key, String value, String creator, String date, String imageSource);

    List<KeyValue> getKeyValueBySource(String source);

    void validateKeyValue(String value, String date, String source, Boolean validated);
}
