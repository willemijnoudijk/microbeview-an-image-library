package nl.bioinf.imagelibrary.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;
import java.util.logging.Logger;

/**
 * @author
 * initialized by Olle
 * edited by Kylie, Olle and Willemijn
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.config.WebSecurityConfig");

    @Autowired
    private DataSource dataSource;

    // TODO: !!! SECURITY DISABLED FOR WEBBASED COURSE ITS PURPOSE !!! REVERT WHEN IN USE !!!

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests().antMatchers("/").permitAll();
    }

//    @Override
//    protected void configure(HttpSecurity httpSecurity) throws Exception {
//        LOGGER.log(Level.ALL,"checks if the given url is validated en secure");
//        httpSecurity
//                .authorizeRequests()
//                    .antMatchers( "/health", "/info","/docs","/api/**" ,"/metadata/**", "/metadata", "/login", "/images/**", "/css/**", "/js/**", "ubuntu/**")
//                    .permitAll()
//                    .anyRequest().authenticated()
//                    .and()
//                .formLogin()
//                    .loginPage("/login")
//                    .defaultSuccessUrl("/home", true)
//                    .permitAll()
//                    .and()
//                .logout()
//                    .permitAll()
//                .and()
//                    .csrf();
//    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth, PasswordEncoder passwordEncoder) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder)
                .usersByUsernameQuery(
                        "select email, password, enabled from users where email = ? and enabled = true")
                .authoritiesByUsernameQuery(
                        "select email, role from users where email = ?");
    }
}