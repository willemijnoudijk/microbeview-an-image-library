package nl.bioinf.imagelibrary.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.logging.Logger;

/**
 * Class that adds more directories to the static resources
 * @author Kylie
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.config.MvcConfig");

    @Value("${json.load.dir}")
    private String libraryPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        LOGGER.info("Directory added to the static resource = " + libraryPath);
        registry
                .addResourceHandler("/" + libraryPath + "/**")
                .addResourceLocations("file:///" + libraryPath + "/",
                                      "file:/" + libraryPath + "/");
    }
}
