package nl.bioinf.imagelibrary.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class HealthCheck implements HealthIndicator {

    @Autowired
    private DataSource dataSource;

    @Override
    public Health health() {
        try {
            Connection connection = dataSource.getConnection();
        } catch (SQLException e) {
            //LOGGER.info("DB not available");
            return Health.down().withDetail("Database is not available", e.getMessage()).build();
        }
        return Health.up().build();
    }
}

