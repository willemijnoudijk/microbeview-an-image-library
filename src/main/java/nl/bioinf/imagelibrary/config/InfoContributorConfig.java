package nl.bioinf.imagelibrary.config;


import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;

import java.util.Collections;

public class InfoContributorConfig implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("test1",
                Collections.singletonMap("test2", "test3"));
    }

}
