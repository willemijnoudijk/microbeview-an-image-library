package nl.bioinf.imagelibrary.config;

import nl.bioinf.imagelibrary.model.ApiError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.util.WebUtils;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

@ControllerAdvice
public class GlobalExceptionHandler {
    /**
     * Provides handling for exceptions throughout this service.
     */
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.data_access.jdbc.GlobalExceptionHandler.java");


    @ExceptionHandler({MethodArgumentTypeMismatchException.class, /*ContentNotAllowedException.class */})
    public final ResponseEntity<ApiError> handleException(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();

        if (ex instanceof MethodArgumentTypeMismatchException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            MethodArgumentTypeMismatchException unfe = (MethodArgumentTypeMismatchException) ex;
            LOGGER.info("Inside MethodArgumentTypeMismatchException");
            return MethodArgumentTypeMismatchException(unfe, headers, status, request);
        } else {
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            return handleExceptionInternal(ex, null, headers, status, request);
        }
    }

    /**
     * Customize the response for MethodArgumentTypeMismatchException.
     */
    protected ResponseEntity<ApiError> MethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = Collections.singletonList(ex.getMessage());
//        LOGGER.info(status.toString());
//        LOGGER.info(errors.get(0));
        return handleExceptionInternal(ex, new ApiError(status, ex.getMessage()), headers, status, request);
    }

    protected ResponseEntity<ApiError> handleExceptionInternal(Exception ex, ApiError body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        }
        LOGGER.info(status.toString());
        LOGGER.info(body.getMessage());
        return new ResponseEntity<>(body, headers, status);
    }
}