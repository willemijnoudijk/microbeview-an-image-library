package nl.bioinf.imagelibrary.config;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Kylie, Olle and Willemijn
 */

public class PasswordEncoderTest {

    public String encodePassword(String password) {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        return passwordEncoder.encode(password);
    }


//    public static void main(String[] args) {
//        String password = "test123";
//
//        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//        String hashedPassword = passwordEncoder.encode(password);
//
//        System.out.println(hashedPassword);
//    }


}