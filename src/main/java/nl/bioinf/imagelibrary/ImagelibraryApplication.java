package nl.bioinf.imagelibrary;

import nl.bioinf.imagelibrary.service.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class ImagelibraryApplication {

    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.ImagelibraryApplication");

    public static void main(String[] args) {
        LOGGER.setLevel(Level.ALL);
        //LOGGER.addHandler(fh);
        //fh.setFormatter(new SimpleFormatter());
        SpringApplication.run(ImagelibraryApplication.class, args);
        LOGGER.info("Starting application..");
    }
}

