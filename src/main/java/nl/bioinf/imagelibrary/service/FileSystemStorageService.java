package nl.bioinf.imagelibrary.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Olle
 */

@Service
public class FileSystemStorageService implements StorageService {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.FileSystemStorageService");

    private final Path rootLocation;

    @Autowired
    public FileSystemStorageService(Environment environment) {
        String uploadDir = Objects.requireNonNull(environment.getProperty("myapp.upload.directory"));
        this.rootLocation = Paths.get(uploadDir);
        LOGGER.info("Upload directory set to " + this.rootLocation);
    }


    private String handleFilename(MultipartFile file) {
        int count = 1;

        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        // if filename does not exist, return the original filename
        if (!this.rootLocation.resolve(filename).toFile().exists()) {
            return filename;
        } else {
            // while filename exists in upload directory execute the following code
            while (this.rootLocation.resolve(filename).toFile().exists()) {
                // split the filename on a dot
                //String[] parts = filename.split("\\.(?=[^.]+$)");
                int index = filename.lastIndexOf(".");
                String part1 = filename.substring(0, index);
                String part2 = filename.substring(index);
                // if part before the extention of filename matches (\d) then run the following code
                if(part1.matches(".+\\((\\d)\\)")) {
                    String newpart = part1.substring(0, part1.length() - 3) + "(" + (count++) + ")";
                    filename = (newpart + part2);
                } else if (part1.matches(".+\\((\\d\\d)\\)")){
                    String newpart = part1.substring(0, part1.length() - 4) + "(" + (count++) + ")";
                    filename = (newpart + part2);
                } else {
                    filename = (part1 + "(" +(count++)+ ")" + part2);
                    LOGGER.info("File renamed to: " + filename);
                }
            }
            return filename;
        }
    }

    @Override
    public String store(MultipartFile file) {
        String filename = handleFilename(file);
        try {
            if (file.isEmpty()) {
                LOGGER.log(Level.WARNING, "exception was thrown, file is empty");
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                LOGGER.log(Level.WARNING, "exception was thrown, file has relative path outside current directory");
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            if (filename.contains("+")) {
                LOGGER.log(Level.WARNING, "exception was thrown, file cannot contain a + character");
                throw new StorageException(
                        "Cannot store file with a '+' in its name, filename = "
                                + filename);
            }

            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, this.rootLocation.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "IOException was catched while trying to store file:" + file.getOriginalFilename());
            throw new StorageException("Failed to store file " + filename, e);
        }
        return filename;
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            LOGGER.log(Level.WARNING, "IOException was catched while trying to initialize storage");
            throw new StorageException("Could not initialize storage", e);
        }
    }
}