package nl.bioinf.imagelibrary.service;

import nl.bioinf.imagelibrary.data_access.KeyValueDataSource;

import nl.bioinf.imagelibrary.model.KeyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Willemijn
 * The service for the key-value pairs database functionality
 */

@Service
public class KeyValueService {
    private final KeyValueDataSource keyValueDataSource;

    @Autowired
    public KeyValueService(KeyValueDataSource keyValueDataSource) {
        this.keyValueDataSource = keyValueDataSource;
    }

    public KeyValue getKeyValueById(String id) {
        return keyValueDataSource.getKeyValueById(id);
    }

    public void storeKeyvalue(String key, String value, String creator, String date, String imageSource) { keyValueDataSource.storeKeyValue(key, value, creator, date, imageSource); }

    public List<KeyValue> getValueBySource(String source) { return keyValueDataSource.getKeyValueBySource(source); }

    public void validateKeyValue(String value, String date, String source, Boolean validated) {this.keyValueDataSource.validateKeyValue(value, date, source, validated);};
}
