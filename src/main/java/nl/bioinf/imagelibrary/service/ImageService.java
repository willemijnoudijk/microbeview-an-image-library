package nl.bioinf.imagelibrary.service;

import nl.bioinf.imagelibrary.data_access.ImageDataSource;
import nl.bioinf.imagelibrary.model.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Willemijn
 * The service for the image database functionality
 */

@Service
public class ImageService {

    private final ImageDataSource imageDataSource;

    @Autowired
    public ImageService(ImageDataSource imageDataSource) {
        this.imageDataSource = imageDataSource;
    }

    public Image getImageById(String id) {
        return imageDataSource.getImageById(id);
    }

    public void storeImage(String fileName, String thumbFileName) {
        imageDataSource.storeImage(fileName, thumbFileName);
    }
}
