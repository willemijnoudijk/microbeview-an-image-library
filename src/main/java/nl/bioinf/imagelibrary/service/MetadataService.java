package nl.bioinf.imagelibrary.service;

import nl.bioinf.imagelibrary.data_access.MetadataDataSource;
import nl.bioinf.imagelibrary.model.Metadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Olle
 * Edited by Willemijn
 */
@Service
public class MetadataService {

    private final MetadataDataSource metadataDataSource;

    @Autowired
    public MetadataService(MetadataDataSource metadataDataSource) {
        this.metadataDataSource = metadataDataSource;
    }

    public ResponseEntity<Metadata> getMetadataById(Integer id) { return metadataDataSource.getMetadataById(id); }

    public List<String> getSourceByTag(String tag) {return metadataDataSource.getSourceByTag(tag);}

    public void storeXyMetadata(Double xcoord, Double ycoord, String tag, String creator, String source, String date) { this.metadataDataSource.storeXyMetadata(xcoord, ycoord, tag, creator, source, date); }

    public void storeGeneralMetadata(String tag, String creator, String source, String date) { this.metadataDataSource.storeGeneralMetadata(tag, creator, source, date); }

    public ResponseEntity<Metadata> getMetadataBySource(String imageSource) { return metadataDataSource.getMetadataBySource(imageSource); }

    public void validateTag(String date_of_creation, String source, Boolean validated, String tag_name) {this.metadataDataSource.validateTag(date_of_creation, source, validated, tag_name);};

    public void removeMetadataInstance(String date_of_creation, String source, String tag_name) { metadataDataSource.removeMetadataInstance(date_of_creation, source, tag_name);}
}
