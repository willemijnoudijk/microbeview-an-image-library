package nl.bioinf.imagelibrary.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Olle
 */

public interface StorageService {

    void init();

    String store(MultipartFile file);

}
