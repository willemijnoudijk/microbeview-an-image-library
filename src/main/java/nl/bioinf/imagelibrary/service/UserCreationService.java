package nl.bioinf.imagelibrary.service;

import nl.bioinf.imagelibrary.data_access.UserCreationDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserCreationService {

    private final UserCreationDataSource userCreationDataSource;

    @Autowired
    public UserCreationService(UserCreationDataSource userCreationDataSource) { this.userCreationDataSource = userCreationDataSource; }

    public void creatUser(String email, String username, String password, String role) { this.userCreationDataSource.createUser(email, username, password, role); }
}
