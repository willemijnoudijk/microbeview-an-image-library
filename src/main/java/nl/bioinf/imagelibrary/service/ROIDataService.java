package nl.bioinf.imagelibrary.service;

import nl.bioinf.imagelibrary.data_access.ROIdataDataSource;
import nl.bioinf.imagelibrary.model.RoiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Service that serves functionality for retrieving ROI information from the database
 *
 * @author Kylie
 */
@Service
public class ROIDataService {
    private final ROIdataDataSource roidataDataSource;

    @Autowired
    public ROIDataService(ROIdataDataSource roidataDataSource) {
        this.roidataDataSource = roidataDataSource;
    }

    public RoiData getRoiDataById(String id) {
        return this.roidataDataSource.getRoiDataById(id);
    }

    public void storeROIData(Double xcoord, Double width, Double ycoord, Double height, String imageSource) {
        this.roidataDataSource.storeROIData(xcoord, width, ycoord, height, imageSource);
    }

    public ArrayList<RoiData> getROIsByImageSource(String imageSource) {
        return this.roidataDataSource.getROIsByImageSource(imageSource);
    }
}
