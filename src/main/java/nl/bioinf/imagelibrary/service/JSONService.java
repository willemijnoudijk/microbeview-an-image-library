package nl.bioinf.imagelibrary.service;

import nl.bioinf.imagelibrary.model.JSONBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author Kylie
 */

@Service
public class JSONService {

    @Value("${json.load.dir}")
    private String libraryPath;
    private final JSONBuilder jsonBuilder = new JSONBuilder();
    public String getJSON() throws IOException {
        return jsonBuilder.getJsonOfDirectoryStructure(libraryPath);
    }
}
