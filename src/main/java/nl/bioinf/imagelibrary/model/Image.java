package nl.bioinf.imagelibrary.model;

/**
 * @author Willemijn
 * Model for image
 */

public class Image {
    private String fileName;
    private String ID;
    private String thumbFileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getThumbFileName() {
        return thumbFileName;
    }

    public void setThumbFileName(String thumbFileName) {
        this.thumbFileName = thumbFileName;
    }

    public Image(String fileName, String thumbFileName) {
        this.fileName = fileName;
        this.thumbFileName = thumbFileName;
    }
}
