package nl.bioinf.imagelibrary.model;

/**
 * @author Olle
 * edited by Willemijn
 */

/**
 * Metadata DTO
 * Used for storing and retrieving metadata
 */
public class Metadata/* extends ResourceSupport*/ {
    private int id;
    private Double xcoord;
    private Double ycoord;
    private String tag;
    private String creator;
    private String source;
    private String date_of_creation;
    private Boolean validated;

    public Metadata() {}

    public Metadata(int id, Double xcoord, Double ycoord, String tag, String creator, String source, String date_of_creation, Boolean validated) {
        this.id = id;
        this.xcoord = xcoord;
        this.ycoord = ycoord;
        this.tag = tag;
        this.creator = creator;
        this.source = source;
        this.date_of_creation = date_of_creation;
        this.validated = validated;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

//    public Integer getId() {
//        return id;
//    }

    public Double getxcoord() {
        return xcoord;
    }

    public Double getycoord() { return ycoord; }

    public String getTag() {
        return tag;
    }

    public String getCreator() {
        return creator;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setxcoord(Double xcoord) {
        this.xcoord = xcoord;
    }

    public void setycoord(Double ycoord) {
        this.ycoord = ycoord;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getDate_of_creation() { return date_of_creation; }

    public void setDate_of_creation(String date_of_creation) { this.date_of_creation = date_of_creation; }

    public Boolean getValidated() {
        return validated;
    }

    public int getId() {
        return id;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

}
