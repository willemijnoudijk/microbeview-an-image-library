package nl.bioinf.imagelibrary.model;

import java.io.File;
import java.util.logging.Logger;

/**
 * Class that creates a new directory from the folder name given by the user.
 * @author Kylie
 */
public class FolderCreator {

    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.model.FolderCreator");

    public static void makeFolder(String currentFolder, String newFolder) {
        final String newFolderPath = currentFolder + "/" + newFolder;
        final File newFolderPathFile = new File(newFolderPath);
        newFolderPathFile.mkdirs();
        LOGGER.info("Created new folder: " + newFolderPath);
    }
}
