package nl.bioinf.imagelibrary.model;

import org.apache.commons.io.FileUtils;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.FilenameUtils;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Willemijn
 * Class which creates a thumbnail of the uploaded images
 */

@Component
public class ThumbnailCreator {

    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.model.ThumbnailCreator");

    // Get location where images are uploaded
    @Value("${myapp.upload.directory}")
    private String jsonLoadDir;

    public String createThumbnail(MultipartFile image) throws IOException {
        LOGGER.log(Level.INFO, "thumbnail creation of image:" + image.getOriginalFilename());
        ByteArrayOutputStream thumbOutput = new ByteArrayOutputStream();
        BufferedImage thumbImg;
        // Create outputfile where thumbnail is created (the original name gets _thumb added to the name
        File outputFile = new File(jsonLoadDir + "/" + FilenameUtils.removeExtension(image.getOriginalFilename()) + "_thumb" + "." + image.getContentType().split("/")[1]);

        BufferedImage img = ImageIO.read(image.getInputStream());

        // The width and height the images are resized to
        int width = (int) (img.getWidth() * 0.8);
        int height = (int) (img.getHeight() * 0.8);

        // Resize and write to file
        thumbImg = Scalr.resize(img, Scalr.Method.AUTOMATIC, width, height);
        ImageIO.write(thumbImg, image.getContentType().split("/")[1] , thumbOutput);

        // Save to file
        FileUtils.writeByteArrayToFile(outputFile, thumbOutput.toByteArray());

        // Return name of the created thumbnail image which is put into the database
        return outputFile.getName();
    }
}
