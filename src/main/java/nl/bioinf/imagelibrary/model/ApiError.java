package nl.bioinf.imagelibrary.model;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ApiError {

    private String status;
    private String message;
    private List<String> errors;

    public ApiError(HttpStatus status, String message) {
        super();
        this.status = status.toString();
        this.message = message;
    }

    public ApiError(HttpStatus status, String message, String error) {
        super();
        this.status = status.toString();
        this.message = message;
    }

    public ApiError(List<String> errors) {
        this.errors = errors;
    }

    public void setStatus(HttpStatus status) {
        this.status = status.toString();
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<String> getErrors() {
        return errors;
    }
}