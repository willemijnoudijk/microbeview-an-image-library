package nl.bioinf.imagelibrary.model;

import java.io.File;

/**
 * Class that changes the filename of the ROI and its thumbnail if already exists.
 * @author Kylie
 */
public class RoiFilename {
    private String newImage;
    private String newThumbnail;
    private int count;

    public RoiFilename(String newImage, String newThumbnail, int count) {
        this.newImage = newImage;
        this.newThumbnail = newThumbnail;
        this.count = count;
    }

    public String getNewImage() {
        return newImage;
    }

    public String getNewThumbnail() {
        return newThumbnail;
    }

    public static final String OS = System.getProperty("os.name").toLowerCase();

    /**
     * Method that does the actual renaming.
     * @return self
     */
    public RoiFilename changeName() {
        while (new File(newImage).exists()) {
            // split the filename on a dot
            int index = newImage.lastIndexOf(".");
            String part1 = newImage.substring(0, index);
            String part2 = newImage.substring(index);
            // if part before the extention of filename matches (\d) then run the following code
            if(part1.matches(".+\\((\\d)\\)")) {
                String newpart = part1.substring(0, part1.length() - 3) + "(" + (count++) + ")";
                createFilenames(part2, newpart);
            } else if (part1.matches(".+\\((\\d\\d)\\)")){
                String newpart = part1.substring(0, part1.length() - 4) + "(" + (count++) + ")";
                createFilenames(part2, newpart);
            } else {
                count++;
                if (OS.contains("win")) {
                    newImage = (part1 + "(" +(count)+ ")" + part2);
                    newThumbnail = (part1 + "(" +(count)+ ")_thumb" + part2);
                }
                else {
                    newImage = (File.separator + part1 + "(" +(count)+ ")" + part2);
                    newThumbnail = (File.separator + part1 + "(" +(count)+ ")_thumb" + part2);
                }
            }
        }
        return this;
    }

    private void createFilenames(String part2, String newpart) {
        if (OS.contains("win")){
            newImage = (newpart + part2);
            newThumbnail = newpart + "_thumb" + part2;
        } else {
            newImage = (File.separator + newpart + part2);
            newThumbnail = File.separator + newpart + "_thumb" + part2;
        }
    }
}