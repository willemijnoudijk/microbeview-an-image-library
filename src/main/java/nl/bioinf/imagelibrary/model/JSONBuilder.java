package nl.bioinf.imagelibrary.model;

import java.io.File;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.logging.Logger;

/**
 * Class that builds a JSON formatted string from the path where the image library is stored.
 * @author
 * initialized by Kylie
 * edited by Willemijn
 */
public class JSONBuilder {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.model.JSONBuilder");
    private static StringBuilder sb = new StringBuilder();

    public String getJsonOfDirectoryStructure(String rootPath) throws IOException {
        traverse(rootPath);
        String returnJSON = sb.toString();
        sb = new StringBuilder();
        return returnJSON;
    }

    /**
     * Method that gets all the subpaths and files by traversing the rootpath
     * @param fullPath the full path to the library
     * @throws IOException
     */
    private static void traverse(String fullPath) throws IOException {
        // check if filepath comes from Windows
        if (fullPath.contains("\\")) {
            String tempFilePath = getFilepathFormat(fullPath);
            sb.append("{\"name\":\"" + tempFilePath + "\"");
        } else {
            sb.append("{\"name\":\"" + fullPath + "\"");
        }
        File parentNode = new File(fullPath);
        if (parentNode.isDirectory()) {
            for (File file : parentNode.listFiles()) {
                if (file.isFile()) {
                    isValidFile(file);
                }
            }
            createStructure(parentNode.listFiles(file -> file.isFile()), "files");
            createStructure(parentNode.listFiles(folder -> folder.isDirectory()), "folders");
        }
        sb.append("}");
    }

    /**
     * Checks if the images are the right type and/or not hidden.
     * @param file
     * @return
     */
    public static void isValidFile(File file) throws IOException {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String mimeType = fileNameMap.getContentTypeFor(file.getName());
        try {
            if (file.isHidden()) {
                if (file.getName().equals(".DS_Store")) {
                    LOGGER.warning("A hidden file was included, but it is only a .DS_Store file automatically created by Mac systems");
                } else {
                    throw new IOException("A hidden file is included");
                }
            }else if (!mimeType.equals("image/png") && !mimeType.equals("image/jpeg") && !mimeType.equals("image/jpg") && !mimeType.equals("image/TIFF")){
                LOGGER.warning("IOException was thrown: not the right image file type was given.");
                throw new IOException("Not a valid file type");
            }
        } catch (NullPointerException ex) {
            LOGGER.warning("NullPointerException was thrown: not a (valid) file was given as image");
        }
    }

    /**
     * Method that creates the JSON formatted structure
     * @param files
     * @param format
     * @throws IOException
     */
    private static void createStructure(File[] files, String format) throws IOException {
        if (files == null || files.length == 0) {
        } else {
            sb.append(",\"" + format + "\":[");
            boolean first = true;
            for (File file: files)  {
                if (first) {
                    first = false;
                } else {
                    sb.append(",");
                }
                traverse(file.toString());
            }
            sb.append("]");
        }
    }

    /**
     * Method that replaces the path separator if it is a path from Windows
     * @param tempFilePath
     * @return
     */
    private static String getFilepathFormat(String tempFilePath) {
        String newFilePath = tempFilePath.replace("\\", "\\\\");
        return newFilePath;
    }
}
