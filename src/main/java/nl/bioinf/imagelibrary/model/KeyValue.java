package nl.bioinf.imagelibrary.model;

/**
 * @author Willemijn
 * Model for key-value
 */

public class KeyValue {
    private int id;
    private String tag_key;
    private String tag_value;
    private String creator;
    private String date;
    private String image_source;
    private String validated;

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public KeyValue(int id, String tag_key, String tag_value, String creator, String date, String image_source) {
        this.id = id;
        this.tag_key = tag_key;
        this.tag_value = tag_value;
        this.creator = creator;
        this.date = date;
        this.image_source = image_source;
        this.validated = validated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage_source() {
        return image_source;
    }

    public void setImage_source(String imageSource) {
        this.image_source = imageSource;
    }

    public String getTag_key() {
        return tag_key;
    }

    public void setTag_key(String tag_key) {
        this.tag_key = tag_key;
    }

    public String getTag_value() {
        return tag_value;
    }

    public void setTag_value(String tag_value) {
        this.tag_value = tag_value;
    }

    public KeyValue() {
    }

    public String getValidated() {
        return validated;
    }

    public void setValidated(String validated) {
        this.validated = validated;
    }

}
