package nl.bioinf.imagelibrary.model;

/**
 * ROIdata object which contains information about created ROIs for an image
 *
 * @author Kylie
 */
public class RoiData {
    private Integer id;
    private Double xcoord;
    private Double width;
    private Double ycoord;
    private Double height;
    private String imageSource;

    public RoiData() {}

    public RoiData(Integer id, Double xcoord, Double width,
                   Double ycoord, Double height, String imageSource) {
        this.id = id;
        this.xcoord = xcoord;
        this.width = width;
        this.ycoord = ycoord;
        this.height = height;
        this.imageSource = imageSource;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getXcoord() {
        return xcoord;
    }

    public void setXcoord(Double xcoord) {
        this.xcoord = xcoord;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getYcoord() {
        return ycoord;
    }

    public void setYcoord(Double ycoord) {
        this.ycoord = ycoord;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    @Override
    public String toString() {
        return "RoiData{" +
                "id=" + id +
                ", xcoord=" + xcoord +
                ", width=" + width +
                ", ycoord=" + ycoord +
                ", height=" + height +
                ", imageSource='" + imageSource + '\'' +
                '}';
    }
}
