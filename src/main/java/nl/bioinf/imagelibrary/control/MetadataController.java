package nl.bioinf.imagelibrary.control;

import nl.bioinf.imagelibrary.model.Metadata;
import nl.bioinf.imagelibrary.service.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class that serves the functionality for the metadata of images
 * @author Olle
 * Edited by Willemijn
 */
@Controller
public class MetadataController {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.MetadataController");

    private final MetadataService metadataService;

    @Autowired
    public MetadataController(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    // TESTING PURPOSES
    @RequestMapping("/metadata/{identifier}")
    public String metaDataById(
            @PathVariable("identifier") Integer identifier,
            Model model) {
        LOGGER.info("processing metadata by ID:" + identifier);
        ResponseEntity<Metadata> example = metadataService.getMetadataById(identifier);
        model.addAttribute("metadata", example);
        return "metadata";
    }

    /*Start of search functionality, to retrieve image source of a given tag. */
    @RequestMapping("/getmetadata/{tag}")
    @ResponseBody
    public List<String> sourceByTag(
            @PathVariable("tag") String tag) {
        List<String> source = metadataService.getSourceByTag(tag);
        return source;
    }

    @RequestMapping("/metadata")
    public String plainMetadata() {
        return "metadata";
    }


    @PostMapping("/storexymetadata")
    public String storeXyMetadata(@RequestParam("leftpx") Double xcoord,
                                  @RequestParam("toppx") Double ycoord,
                                  @RequestParam("tag") String tag,
                                  @RequestParam("imageName") String source,
                                  Principal principal) {
        LOGGER.info("Image source: " + source + " of tag: " + tag + " by creator: " + principal.getName());
        String creator = principal.getName();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
        Date date = new Date();
        metadataService.storeXyMetadata(xcoord, ycoord, tag, creator, source, dateFormat.format(date));
        return "redirect:/";
    }

    @PostMapping("/storegeneralmetadata")
    public String storeGeneralMetadata(
                                @RequestParam("tag") String tag,
                                @RequestParam("imageName") String source,
                                Principal principal) {
        LOGGER.info("general metadata processed: no x- and y-coordinate");
        String creator = principal.getName();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
        Date date = new Date();
        metadataService.storeGeneralMetadata(tag, creator, source, dateFormat.format(date));
        return "redirect:/";
    }

    @RequestMapping("/showmetadata/{image_source}")
    @ResponseBody
    public ResponseEntity<Metadata> showMetadata(
            @PathVariable("image_source") String imageSource) {
        ResponseEntity<Metadata> result = metadataService.getMetadataBySource(imageSource);
        return result;
    }

    @PostMapping("/validatetag/{image_source}")
    public String storeGeneralMetadata(
            @RequestParam("validated") Boolean validated,
            @RequestParam("date_of_creation") String date_of_creation,
            @RequestParam("tag_name") String tag_name,
            @PathVariable("image_source") String source) {
        metadataService.validateTag(date_of_creation, source, validated, tag_name);
        LOGGER.info("tag has been validated");
        return "redirect:/";
    }

    @PostMapping("/removemetadata/{image_source}")
    public String removeMetadataInstance(
            @RequestParam("date_of_creation") String date_of_creation,
            @RequestParam("tag_name") String tag_name,
            @PathVariable("image_source") String source) {
        metadataService.removeMetadataInstance(date_of_creation, source, tag_name);
        return "redirect:/";
    }
}