package nl.bioinf.imagelibrary.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.io.File;
import java.util.logging.Logger;

/**
 * Controller that serves the functionality for deleting a selected image.
 *
 * @author Kylie
 */
@Controller
public class ImageDeletingController {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.ImageDeletingController");

    @RequestMapping(value = "/imagedeleting", method = RequestMethod.POST)
    public RedirectView handleDeleteImage(@RequestParam("selectedImage") String selectedImage,
                                          @RequestParam("thumbnailSource") String thumbnailSource) {
        deleteImage(selectedImage, thumbnailSource);
        return new RedirectView("/");
    }

    /**
     * Method that deletes the selected image and its thumbnail.
     * @param selectedImage
     * @param thumbnailSource
     */
    private void deleteImage(@RequestParam("selectedImage") String selectedImage,
                             @RequestParam("thumbnailSource") String thumbnailSource) {
        File selectedFile = new File(File.separator + selectedImage);
        File selectedThumb = new File(File.separator + thumbnailSource);
        selectedFile.delete();
        selectedThumb.delete();

        LOGGER.info("Successfully deleted image '" + selectedFile + "'");
    }
}
