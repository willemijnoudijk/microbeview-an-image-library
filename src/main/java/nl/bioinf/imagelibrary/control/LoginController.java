package nl.bioinf.imagelibrary.control;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller that serves the functionality for log-in
 *
 * @author Kylie, Olle and Willemijn
 */
@Controller
public class LoginController {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.LoginController");

    @RequestMapping(value="/login")
    public String showForm() throws IOException {
        LOGGER.log(Level.ALL,"Handling the /login request");
        return "/login";
    }
}