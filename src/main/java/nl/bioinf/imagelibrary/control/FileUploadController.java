package nl.bioinf.imagelibrary.control;

import nl.bioinf.imagelibrary.model.ThumbnailCreator;
import nl.bioinf.imagelibrary.service.ImageService;
import nl.bioinf.imagelibrary.service.StorageFileNotFoundException;
import nl.bioinf.imagelibrary.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Olle
 * edited by Willemijn
 */

@Controller
public class FileUploadController {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.FileUploadController");

    private final StorageService storageService;
    private final ImageService imageService;
    private final ThumbnailCreator thumbnailCreator;

    @Autowired
    public FileUploadController(StorageService storageService, ImageService imageService, ThumbnailCreator thumbnailCreator) {
        this.storageService = storageService;
        this.imageService = imageService;
        this.thumbnailCreator = thumbnailCreator;
    }

    @PostMapping("/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile[] file) throws IOException {
        LOGGER.log(Level.ALL, "Handling file upload");
        try {
            for (MultipartFile multipartFile : file) {
                LOGGER.info("handling file upload of file:" + multipartFile.getOriginalFilename());
                String fileName = storageService.store(multipartFile);
                MultipartFile multipartFileToCreateThumbnail = convertFileToMultipartFile(fileName);
                String thumbnailName = thumbnailCreator.createThumbnail(multipartFileToCreateThumbnail);
                imageService.storeImage(multipartFile.getOriginalFilename(), thumbnailName);
            }
        } catch (StorageFileNotFoundException exc) {
            handleStorageFileNotFound(exc);
        }
        return "redirect:/";
    }

    /**
     * Method which converts a file (String) to a multipart file, to be later used for thumbnail creation
     */
    @Value("${myapp.upload.directory}")
    private String jsonLoadDir;
    private MultipartFile convertFileToMultipartFile(String fileName) throws IOException {
        Path path = Paths.get(jsonLoadDir + "/" + fileName);
        LOGGER.info("filename = " + fileName);
        String name = fileName;
        String originalFileName = fileName;

        // Get extension for contenttype of image
        int index = fileName.lastIndexOf(".") + 1;
        String extension = fileName.substring(index);
        LOGGER.info("extension = " + extension);

        String contentType = "image/" + extension;
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        }
        catch (IOException e) {
            LOGGER.log(Level.WARNING, "exception catched while trying to create a multipartfile of file: " + fileName);
            throw new IOException("Exception catched when trying to create a multipartfile");
        }
        catch (NullPointerException ex) {
            System.out.println("The file given to convert to multipartfile was null");
        }

        return new MockMultipartFile(name, originalFileName, contentType, content);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }
}
