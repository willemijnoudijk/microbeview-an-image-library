package nl.bioinf.imagelibrary.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.io.File;
import java.util.logging.Logger;

/**
 * Controller that serves the functionality for renaming a selected folder
 *
 * @author Kylie
 */
@Controller
public class ImageRenamingController {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.ImageRenamingController");
    @RequestMapping(value = "/imagerenaming", method = RequestMethod.POST)
    public RedirectView handleImageRename(@RequestParam("selectedImage") String selectedImage,
                                          @RequestParam("thumbnailSource") String thumbnailSource,
                                          @RequestParam("newImageName") String newImageName) {
        renameImage(selectedImage, thumbnailSource, newImageName);
        return new RedirectView("/");
    }

    /**
     * Method that renames the selected image and its thumbnail to the new given image name.
     * @param selectedImage
     * @param thumbnailSource
     * @param newImageName
     */
    private void renameImage(@RequestParam("selectedImage") String selectedImage,
                             @RequestParam("thumbnailSource") String thumbnailSource,
                             @RequestParam("newImageName") String newImageName) {
        // Get the extension and path from the image
        int extensionIndex = selectedImage.split("\\.").length - 1;
        String extension = "." + selectedImage.split("\\.")[extensionIndex];
        String path = new File(selectedImage).getParent();

        String newImage = File.separator + path + File.separator + newImageName + extension;
        String newThumbnail = File.separator + path + File.separator + newImageName + "_thumb" + extension;

        // Do the renaming
        new File(File.separator + selectedImage).renameTo(new File(newImage));
        new File(File.separator + thumbnailSource).renameTo(new File(newThumbnail));

        LOGGER.info("Renamed image '" + selectedImage + "' to '" + newImageName + extension + "'");
    }
}
