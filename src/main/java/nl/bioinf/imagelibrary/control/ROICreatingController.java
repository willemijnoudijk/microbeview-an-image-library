package nl.bioinf.imagelibrary.control;

import nl.bioinf.imagelibrary.model.RoiData;
import nl.bioinf.imagelibrary.model.RoiFilename;
import nl.bioinf.imagelibrary.service.ROIDataService;
import org.apache.commons.io.FileUtils;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller that serves the fuctionality for creating ROIs from the selected image
 *
 * @author Kylie
 */
@Controller
public class ROICreatingController {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.ROICreatingController");
    public static final String OS = System.getProperty("os.name").toLowerCase();
    private final ROIDataService roiDataService;

    @Autowired
    ROICreatingController(ROIDataService roiDataService){
        this.roiDataService = roiDataService;
    }

    @RequestMapping(value = "/storeRoiCoordinates", method = RequestMethod.POST)
    public RedirectView storeROIdata(@RequestParam("ROIdata") ArrayList roiData,
                                     @RequestParam("imageSource") String imageSource) {

        Double xcoord = Double.parseDouble(roiData.get(0).toString());
        Double width = Double.parseDouble(roiData.get(1).toString());
        Double ycoord = Double.parseDouble(roiData.get(2).toString());
        Double height = Double.parseDouble(roiData.get(3).toString());

        LOGGER.log(Level.ALL, "Stored ROI coordinates in the database");
        roiDataService.storeROIData(xcoord, width, ycoord, height, imageSource);

        return new RedirectView("/");
    }

    @RequestMapping(value = "/showROIs/{image_source}")
    @ResponseBody
    public ArrayList<RoiData> getROIsByImageSource(@PathVariable("image_source") String imageSource) {
        imageSource = imageSource.replace("+", "/");

        ArrayList<RoiData> roiData = roiDataService.getROIsByImageSource(imageSource);

        return roiData;
    }

    @RequestMapping(value = "/ROIcreation", method = RequestMethod.POST)
    public RedirectView handleCreatedROI(@RequestParam("selectedROI") MultipartFile multipartFile,
                                         @RequestParam("originalImage") String originalImage,
                                         @RequestParam("imageName") String imageName) throws IOException {

        // Create the path and filename for the ROI and its thumbnail
        int extensionIndex = originalImage.split("\\.").length - 1;
        String extension = "." + originalImage.split("\\.")[extensionIndex];
        String path = new File(originalImage).getParent();
        String newImage;
        String newThumbnail;
        if (OS.contains("win")) {
            // if system is Windows
            newImage = path + File.separator + imageName + "_ROI" + extension;
            newThumbnail = path + File.separator + imageName + "_ROI_thumb" + extension;
        } else {
            newImage = File.separator + path + File.separator + imageName + "_ROI" + extension;
            newThumbnail = File.separator + path + File.separator + imageName + "_ROI_thumb" + extension;
        }

        int count = 1;
        if (!new File(newImage).exists()) {
            // Create thumbnail and new image of the ROI
            createThumbnail(multipartFile, newThumbnail);
            multipartFile.transferTo(Paths.get(newImage));
            LOGGER.info("New ROI created for image " + originalImage);
        } else {
            // Change the filename and then create new thumbnail and image of the ROI
            RoiFilename RoiFilename = new RoiFilename(newImage, newThumbnail, count).changeName();
            newImage = RoiFilename.getNewImage();
            newThumbnail = RoiFilename.getNewThumbnail();
            createThumbnail(multipartFile, newThumbnail);
            multipartFile.transferTo(Paths.get(newImage));
            LOGGER.info("New ROI created for image " + originalImage);
        }

        return new RedirectView("/");
    }

    /**
     * Class that creates the thumbnail image of the selected ROI.
     * @param multipartFile
     * @param newThumbnail
     * @throws IOException
     */
    private void createThumbnail(@RequestParam("selectedROI") MultipartFile multipartFile,
                                 String newThumbnail) throws IOException {
        File thumbnail = new File(newThumbnail);
        ByteArrayOutputStream thumbOutput = new ByteArrayOutputStream();
        BufferedImage thumbImg = ImageIO.read(multipartFile.getInputStream());

        // The width and height the images are resized to
        int width = (int) (thumbImg.getWidth() * 0.8);
        int height = (int) (thumbImg.getHeight() * 0.8);

        // Resize and write to file
        BufferedImage defThumbImg = Scalr.resize(thumbImg, Scalr.Method.AUTOMATIC, width, height);
        ImageIO.write(defThumbImg, multipartFile.getContentType().split("/")[1] , thumbOutput);

        // Save to file
        FileUtils.writeByteArrayToFile(thumbnail, thumbOutput.toByteArray());
    }
}
