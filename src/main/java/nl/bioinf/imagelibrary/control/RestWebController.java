package nl.bioinf.imagelibrary.control;

import nl.bioinf.imagelibrary.service.JSONService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller that serves the functionality for getting all the folders and images.
 *
 * @author Kylie
 */
@RestController
public class RestWebController {

    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.RestWebController");

    @Autowired
    JSONService jsonService;

    @GetMapping(value = "/imagelibrary")
    public String JSONlibrary() throws IOException {
        LOGGER.log(Level.ALL, "Getting the JSON data");
        return jsonService.getJSON();
    }
}
