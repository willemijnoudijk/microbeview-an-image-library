package nl.bioinf.imagelibrary.control;

import nl.bioinf.imagelibrary.service.UserCreationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class UserCreationController {
    private final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.UserCreationController");

    private final UserCreationService userCreationService;

    @Autowired
    public UserCreationController(UserCreationService userCreationService) {
        this.userCreationService = userCreationService;
    }

    @GetMapping("/create-user")
    public String showPage(Model model, Authentication auth) throws IOException {

        Object authority = auth.getAuthorities().toArray()[0];
        model.addAttribute("role", authority.toString());

        LOGGER.log(Level.ALL,"Handling the /create-user request");
        return "createUser";
    }

    @PostMapping("/create")
    public String createUser(@RequestParam("new-email") String email,
                           @RequestParam("new-username") String username,
                           @RequestParam("new-password") String password,
                           @RequestParam("new-role") String role) {
        LOGGER.info("About to create new user..");
        userCreationService.creatUser(email, username, password, role);
        return "redirect:/";
    }
}