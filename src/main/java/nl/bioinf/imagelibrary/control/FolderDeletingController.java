package nl.bioinf.imagelibrary.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.logging.Logger;

/**
 * Controller that serves the functionality for deleting a selected folder.
 *
 * @author Kylie
 */
@Controller
public class FolderDeletingController {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.FolderDeletingController");

    @RequestMapping(value = "/folderdeleting", method = RequestMethod.POST)
    public RedirectView handleDeleteFolder(@RequestParam("selectedFolder") String selectedFolder) throws IOException {
        deleteFolder(selectedFolder);
        return new RedirectView("/");
    }

    /**
     * Method that deletes the selected folder with all its content.
     * @param selectedFolder
     * @throws IOException
     */
    private void deleteFolder(@RequestParam("selectedFolder") String selectedFolder) throws IOException {
        Path path = Paths.get(selectedFolder);
        Files.walk(path)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
        LOGGER.info("Deleted folder '" + selectedFolder + "' with all its content");
    }
}
