package nl.bioinf.imagelibrary.control;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Kylie, Olle and Willemijn
 */

@Controller
public class HomeController {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.HomeController");
    @RequestMapping(value = {"/home", "/"})
    public String home(Model model, Authentication auth, Principal principal) {
        LOGGER.log(Level.ALL, "Handling the /home request after user is logged on");

        Object authority = auth.getAuthorities().toArray()[0];
        LOGGER.info("User its authority: " + authority.toString());
        model.addAttribute("role", authority.toString());
        model.addAttribute("username", principal.getName());
        LOGGER.info("Principal information: " + auth.getPrincipal());

        return "home"; }
}
