package nl.bioinf.imagelibrary.control;

import nl.bioinf.imagelibrary.model.FolderCreator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller that serves the functionality for creating a new folder in the current directory
 *
 * @author Kylie
 */
@Controller
public class FolderCreationController {

    @RequestMapping(value = "/foldercreation", method = RequestMethod.POST)
    public RedirectView newFolder(@RequestParam("currentFolder") String currentFolder,
                                  @RequestParam("newFolder") String newFolder) {
        FolderCreator.makeFolder(currentFolder, newFolder);
        return new RedirectView("/");
    }
}
