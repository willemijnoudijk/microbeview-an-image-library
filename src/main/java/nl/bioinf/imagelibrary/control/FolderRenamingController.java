package nl.bioinf.imagelibrary.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.io.File;
import java.util.logging.Logger;

/**
 * Controller that serves the functionality for renaming a folder
 *
 * @author Kylie
 */
@Controller
public class FolderRenamingController {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.FolderRenamingController");

    @RequestMapping(value = "/folderrenaming", method = RequestMethod.POST)
    public RedirectView newFolderName(@RequestParam("selectedFolder") String selectedFolder,
                                      @RequestParam("newFolderName") String newFolderName) {
        rename(selectedFolder, newFolderName);
        return new RedirectView("/");
    }

    /**
     * Method that renames the selected folder to the new given folder name.
     * @param selectedFolder
     * @param newFolderName
     */
    public void rename(@RequestParam("selectedFolder") String selectedFolder,
                        @RequestParam("newFolderName") String newFolderName) {
        File selectedDir = new File(selectedFolder);
        File newDirName = new File(selectedDir.getParent() + File.separator + newFolderName);
        System.out.println("selectedDir = " + selectedDir);
        System.out.println("newDirName = " + newDirName);
        selectedDir.renameTo(newDirName);
        LOGGER.info("Renamed folder '" + selectedDir.getName() + "' to '" + newFolderName + "'");
    }
}
