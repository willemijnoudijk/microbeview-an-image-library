package nl.bioinf.imagelibrary.control;

import nl.bioinf.imagelibrary.model.Metadata;
import nl.bioinf.imagelibrary.service.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

/**
 * @author Olle
 * WEBBASED INFO 2 PART
 */
@RestController
@RequestMapping("/api/metadata/v1")
public class MetadataRestController {
    private static final Logger LOGGER = Logger.getLogger("nl.bioinf.imagelibrary.control.MetadataController");

    private final MetadataService metadataService;

    @Autowired
    public MetadataRestController(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    @GetMapping("/get/by/id")
    public ResponseEntity<Metadata> metadataEndpointById(
            @RequestParam("id") Integer identifier) {

        // ask the data using database instance id via (C) MetadataService -> (I) MetadataDataSource -> (C) MetadataDataSourceJdbc )
        return new ResponseEntity(metadataService.getMetadataById(identifier), HttpStatus.OK);
    }

    @GetMapping("/get/by/source")
    public ResponseEntity<Metadata> metadataEndpointBySource(
            @RequestParam("source") String source) {

        // ask the data using image source via (C) MetadataService -> (I) MetadataDataSource -> (C) MetadataDataSourceJdbc )
        return new ResponseEntity(metadataService.getMetadataBySource(source), HttpStatus.OK);
    }

    @PostMapping("/validate/tag")
    public void metadataValidateTag(
            @RequestParam("validated") Boolean validated,
            @RequestParam("date_of_creation") String date_of_creation,
            @RequestParam("tag_name") String tag_name,
            @RequestParam("image_source") String source) {

        // change validation field from false to true via (C) MetadataService -> (I) MetadataDataSource -> (C) MetadataDataSourceJdbc )
        metadataService.validateTag(date_of_creation, source, validated, tag_name);
        LOGGER.info("tag has been validated");
    }
}