package nl.bioinf.imagelibrary.model;


import org.junit.Assert;
import org.junit.Test;
//import org.springframework.test.util.AssertionErrors;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author
 * initialized by Willemijn
 * edited by Kylie
 */

public class JSONBuilderTest {
    JSONBuilder jsonBuilder = new JSONBuilder();

    @Test
    public void getJSONwindows() throws IOException {
        String rootPath = "C:\\Users\\kylie\\Documents\\projectimagelibraryui\\src\\main\\resources\\static\\images\\example";
        System.out.println(jsonBuilder.getJsonOfDirectoryStructure(rootPath));
    }

    @Test
    public void getJSON() throws IOException {
        String rootPath = "/homes/wfoudijk/thema11/Project/projectimagelibraryui/src/main/resources/static/images/uploads/";
        System.out.println(jsonBuilder.getJsonOfDirectoryStructure(rootPath));
    }

    @Test
    public void isValidFileSunny() throws IOException {
        File sunny = new File("C:\\Users\\willemijnoudijk\\Documents\\BFV3\\ProjectMinor\\src\\main\\resources\\static\\images\\Coccidia\\img\\13-03-2019\\0001.jpeg");
        jsonBuilder.isValidFile(sunny);
    }

    @Test
    public void isValidFileHidden() throws IOException {
        File hidden = new File("/homes/wfoudijk/thema11/Project/projectimagelibraryui/.DS_Store");
        Throwable exception = assertThrows(IOException.class, () -> jsonBuilder.isValidFile(hidden));
        assertEquals("A hidden file is included", exception.getMessage());
    }

    @Test
    public void isValidFileWrongExtension() throws IOException {
        File dir = new File("/homes/wfoudijk/thema11/Project/projectimagelibraryui/src/main/resources/static/css/throbber.gif");
        Throwable exception = assertThrows(IOException.class, () -> jsonBuilder.isValidFile(dir));
        assertEquals("Not a valid file type", exception.getMessage());
    }
    @Test
    public void isValidFileNull() throws IOException {
        try {
            jsonBuilder.isValidFile(null);
            Assert.fail("Fail! Method was expected to throw an exception because null file was given");
        } catch (NullPointerException e) {
            // expected
        }
    }
}
