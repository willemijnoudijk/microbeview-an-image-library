package nl.bioinf.imagelibrary.model;

import org.junit.jupiter.api.Test;

/**
 * Test class that tests the functionality for creating a new folder
 *
 * @author Kylie
 */
class FolderCreatorTest {

    @Test
    void makeFolder() {
        String currentFolder = "C:/Users/kylie/Pictures/MicrobeView/Images";
        String newFolder = "currentDate";
        FolderCreator.makeFolder(currentFolder, newFolder);
    }

    @Test
    void makeFolderDuplicate() {
        String currentFolder = "C:/Users/kylie/Pictures/MicrobeView/Images";
        String newFolder = "currentDate";
        FolderCreator.makeFolder(currentFolder, newFolder);
    }
}