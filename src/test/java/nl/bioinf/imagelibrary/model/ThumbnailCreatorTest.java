package nl.bioinf.imagelibrary.model;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for thumbnail creation
 * @author Willemijn 
 * */

class ThumbnailCreatorTest {
    ThumbnailCreator thumbnailCreator= new ThumbnailCreator();

    @Test
    void createThumbnailSunny() throws IOException {
        Path path = Paths.get("/homes/wfoudijk/thema11/Project/projectimagelibraryui/src/main/resources/static/images/uploads/0083.jpg");
        String name = "0083.jpg";
        String originalFileName = "0083.jpg";
        String contentType = "image/jpg";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MultipartFile result = new MockMultipartFile(name,
                originalFileName, contentType, content);

        String thumbnail = thumbnailCreator.createThumbnail(result);
        assertEquals("0083_thumb.jpg", thumbnail);
    }

    @Test
    void createThumbnailDuplicate() throws IOException {
        Path path = Paths.get("/homes/wfoudijk/thema11/Project/projectimagelibraryui/src/main/resources/static/images/uploads/0011(1).jpg");
        String name = "0011(1).jpg";
        String originalFileName = "0011(1).jpg";
        String contentType = "image/jpg";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MultipartFile result = new MockMultipartFile(name,
                originalFileName, contentType, content);

        String thumbnail = thumbnailCreator.createThumbnail(result);
        assertEquals("0011(1)_thumb.jpg", thumbnail);
    }
}