package nl.bioinf.imagelibrary.control;

import nl.bioinf.imagelibrary.ImagelibraryApplication;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Olle
 * WEBBASED INFO 2 PART
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ImagelibraryApplication.class)
@AutoConfigureMockMvc
public class MetadataRestControllerTest {

    @Autowired
    public MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    // set snippets output directory
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("build/generated-snippets");
    private RestDocumentationResultHandler document;

    /**
     * configure and set up needed components
     * for further usage in tests
     */
    @Before
    public void setUp() {
        this.document = document("outputs",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint()));
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .alwaysDo(this.document)
                .build();
    }


    /**
     * This test tests the outcome of requesting the metadata
     * using a database identifier. Since a database id is unique
     * there should be only one stand-alone JSON object
     * @throws Exception
     */
    @Test
    public void metadataEndpointByIdSunny() throws Exception {
        String responseJson = "{\"id\":2,\"xcoord\":0.5336,\"ycoord\":0.3636,\"tag\":\"test2\",\"creator\":\"willemijn@admin.nl\",\"source\":\"0083.jpg\",\"date_of_creation\":\"28/05/2019 - 12:45\",\"validated\":false}";
        this.mockMvc.perform(get("/api/metadata/v1/get/by/id?id={id}&",
                "2"))
                .andExpect(status().isOk())
                .andExpect(content().json(responseJson))
                .andDo(this.document.document(
                        requestParameters(
                                parameterWithName("id").description("The database instance identifier by which the metadata is retrieved"))
                ));
    }

    /**
     * This test tests the outcome of requesting the metadata
     * using a database identifier which is not of type Integer but String.
     * @throws Exception
     */
    @Test
    public void metadataEndpointByIdUsingString() throws Exception {
        String responseJson = "{\"status\":\"400 BAD_REQUEST\",\"message\":\"Failed to convert value of type 'java.lang.String' to required type 'java.lang.Integer'; nested exception is java.lang.NumberFormatException: For input string: \\\"z\\\"\",\"errors\":null}";
        this.mockMvc.perform(get("/api/metadata/v1/get/by/id?id={id}&",
                "z"))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(responseJson))
                .andDo(this.document.document(
                        requestParameters(
                                parameterWithName("id").description("The database instance identifier by which the metadata is retrieved"))
                ));
    }


    /**
     * This test tests the outcome of requesting the metadata
     * using a database identifier which is not of type Integer but Float.
     * @throws Exception
     */
    @Test
    public void metadataEndpointByIdUsingFloat() throws Exception {
        String responseJson = "{\"status\":\"400 BAD_REQUEST\",\"message\":\"Failed to convert value of type 'java.lang.String' to required type 'java.lang.Integer'; nested exception is java.lang.NumberFormatException: For input string: \\\"5.5\\\"\",\"errors\":null}";
        this.mockMvc.perform(get("/api/metadata/v1/get/by/id?id={id}&",
                "5.5"))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(responseJson))
                .andDo(this.document.document(
                        requestParameters(
                                parameterWithName("id").description("The database instance identifier by which the metadata is retrieved"))
                ));
    }

    /**
     * This test tests the outcome of requesting the metadata
     * using an image source / filename. Since it git gives a list
     * the values are retrieved using " [].keyname "
     * @throws Exception
     */
    @Test
    public void metadataEndpointBySourceSunny() throws Exception {
        String responseJson = "[{\"id\":1,\"xcoord\":0.3336,\"ycoord\":0.3336,\"tag\":\"test1\",\"creator\":\"willemijn@admin.nl\",\"source\":\"0083.jpg\",\"date_of_creation\":\"28/05/2019 - 12:45\",\"validated\":false},{\"id\":2,\"xcoord\":0.5336,\"ycoord\":0.3636,\"tag\":\"test2\",\"creator\":\"willemijn@admin.nl\",\"source\":\"0083.jpg\",\"date_of_creation\":\"28/05/2019 - 12:45\",\"validated\":false},{\"id\":3,\"xcoord\":0.824,\"ycoord\":0.2453,\"tag\":\"test3\",\"creator\":\"willemijn@admin.nl\",\"source\":\"0083.jpg\",\"date_of_creation\":\"28/05/2019 - 12:45\",\"validated\":false},{\"id\":4,\"xcoord\":0.57,\"ycoord\":0.765,\"tag\":\"test4\",\"creator\":\"willemijn@admin.nl\",\"source\":\"0083.jpg\",\"date_of_creation\":\"28/05/2019 - 12:45\",\"validated\":false},{\"id\":5,\"xcoord\":0.88,\"ycoord\":0.89,\"tag\":\"test5\",\"creator\":\"willemijn@admin.nl\",\"source\":\"0083.jpg\",\"date_of_creation\":\"28/05/2019 - 12:45\",\"validated\":false},{\"id\":6,\"xcoord\":0.0,\"ycoord\":0.0,\"tag\":\"test6\",\"creator\":\"olle@admin.nl\",\"source\":\"0083.jpg\",\"date_of_creation\":\"28/05/2019 - 12:45\",\"validated\":false}]";
        this.mockMvc.perform(get("/api/metadata/v1/get/by/source?source={source}&",
                "0083.jpg"))
                .andExpect(status().isOk())
                .andExpect(content().json(responseJson))
                .andDo(this.document.document(
                        requestParameters(
                                parameterWithName("source").description("The image filename/source-name by which the metadata is retrieved")),
                        responseFields(
                                fieldWithPath("[].id").description("The database instance identifier"),
                                fieldWithPath("[].xcoord").description("The x-coordinate of the metadata tag. This is used to display the tag on the image."),
                                fieldWithPath("[].ycoord").description("The y-coordinate of the metadata tag. This is used to display the tag on the image."),
                                fieldWithPath("[].tag").description("The name of the metadata tag, given by the creator of the tag."),
                                fieldWithPath("[].creator").description("The creator's email-address."),
                                fieldWithPath("[].source").description("The filename (source) of the image the metadata tag is attached to."),
                                fieldWithPath("[].date_of_creation").description("The date on which the tag has been created."),
                                fieldWithPath("[].validated").description("Tells if the tag is validated or not."))
                ));
    }

    /**
     * This test tests the outcome of requesting the metadata
     * using an image source / filename for which no data is available.
     * @throws Exception
     */
    @Test
    public void metadataEndpointBySourceNoDataAvailable() throws Exception {
        String responseJson = "{\"headers\":{},\"body\":\"NO METADATA AVAILABLE\",\"statusCode\":\"OK\",\"statusCodeValue\":200}";
        this.mockMvc.perform(get("/api/metadata/v1/get/by/source?source={source}&",
                "0055.jpg"))
                .andExpect(status().isOk())
                .andExpect(content().json(responseJson));
    }

    /**
     * This test tests the outcome of requesting the metadata
     * using an image source / filename. Since this image has
     * no excising metadata, it returns an empty list.
     * @throws Exception
     */
    @Test
    public void metadataEndpointBySourceNoMetadata() throws Exception {
        String responseJson = "[]";
        this.mockMvc.perform(get("/api/metadata/v1/get/by/source?source={source}&",
                "0085.jpg"))
                .andExpect(status().isOk())
                .andExpect(content().json(responseJson))
                .andDo(this.document.document(
                        requestParameters(
                                parameterWithName("source").description("The image filename/source-name by which the metadata is retrieved"))
                ));
    }

    /**
     * This test tests the outcome of the POST request which
     * validates a tag given parameters which can identify the
     * database instance you are aiming for.
     * @throws Exception
     */
    @Test
    public void metadataValidateTagSunny() throws Exception {
        this.mockMvc.perform(post("/api/metadata/v1/validate/tag?validated={validated}&date_of_creation={date}&tag_name={tagname}&image_source={source}",
                "true","28/05/2019 - 12:45","ecoli2","0084.jpg"))
                .andExpect(status().isOk())
                .andDo(this.document.document(
                        requestParameters(
                                parameterWithName("validated").description("The image filename/source-name by which the metadata is retrieved"),
                                parameterWithName("tag_name").description("The image filename/source-name by which the metadata is retrieved"),
                                parameterWithName("date_of_creation").description("The image filename/source-name by which the metadata is retrieved"),
                                parameterWithName("image_source").description("The image filename/source-name by which the metadata is retrieved")))
                );
    }

}